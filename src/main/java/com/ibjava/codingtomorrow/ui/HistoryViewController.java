package com.ibjava.codingtomorrow.ui;

import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.history.HistoryManager;
import com.ibjava.codingtomorrow.test.TerminalTest;
import com.ibjava.codingtomorrow.ui.display.GameDisplay;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXToggleButton;
import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.binding.When;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class HistoryViewController implements Initializable {

    /*PROGRESS*/
    @FXML TitledPane loadProgressTitledPane;
    private Service<Void> singleRunsLoaderService = createRunLoaderService();
    @FXML Label singleRunProgressLabel; // service status label
    static final ObservableList<Game> SINGLE_RUNS_LIST = FXCollections.observableArrayList(); // list bound to the list view

    private Service<Void> terminalTestLoaderService = createTerminalTestLoaderService();
    @FXML Label terminalTestProgressLabel; // service status label
    private static final ObservableList<TerminalTest> TERMINAL_TESTS_LIST = FXCollections.observableArrayList(); // list bound to the combo box

    /*CONTROLS*/
    @FXML TitledPane controlsTitledPane;
    @FXML JFXToggleButton runsOrTestsToggle; // for TerminalTest load on/off
    @FXML JFXComboBox<TerminalTest> terminalTestComboBox; // for choosing TerminalTest
    @FXML JFXToggleButton gameFilterToggle; // for filtering on/off
    @FXML JFXComboBox<String> gameFilterComboBox; // for choosing filtered position

    /*GAMES*/
    @FXML TitledPane gamesTitledPane;
    // displays a list of games, either from SINGLE_RUNS_LIST or from TERMINAL_TESTS_LIST's selection
    @FXML ListView<Game> gameHistoryListView;

    /*DISPLAY*/
    @FXML TitledPane gameDisplayTitledPane;
    private GameDisplay gameDisplay = new GameDisplay(MainViewController.BOARD_UI); // for animation support


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // start services
        singleRunsLoaderService.start();
        terminalTestLoaderService.start();

        // add custom component to the scene
        gameDisplayTitledPane.setContent(gameDisplay);

        initListListeners();
        initRunsOrTestsToggle();
        initGameFilterToggle();
        initTerminalTestComboBox();
        initGameHistoryListView();
    }

    /**
     * When the static lists are updated then update their UI element as well
     */
    private void initListListeners() {
        SINGLE_RUNS_LIST.addListener((ListChangeListener<? super Game>) c ->
        {
            if (gameHistoryListView != null) gameHistoryListView.refresh();
        });

        TERMINAL_TESTS_LIST.addListener((ListChangeListener<? super TerminalTest>) c ->
        {
            if (terminalTestComboBox != null) terminalTestComboBox.setItems(TERMINAL_TESTS_LIST);
        });
    }

    /**
     * - text property binding
     * - listener to update the gameHistoryListViews content
     */
    private void initRunsOrTestsToggle() {
        // binding for dynamic label update
        StringBinding stringBinding = // automatically changing toggle text
                new When(runsOrTestsToggle.selectedProperty())
                        .then("Single")
                        .otherwise("Tests");
        runsOrTestsToggle.textProperty().bind(stringBinding);

        // data list change in list view based on selection
        runsOrTestsToggle.selectedProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue) { // if changed to tests
                TerminalTest selectedTest = terminalTestComboBox.getValue(); // get selected test
                ObservableList<Game> games = FXCollections.observableArrayList();
                if (selectedTest != null) games.addAll(selectedTest.getGameList()); // check  for null selection

                gameHistoryListView.setItems(games); // if selection eas null then the list view will be empty
            } else { // if changed to single runs
                gameHistoryListView.setItems(SINGLE_RUNS_LIST); // change to single runs
            }
        });
    }

    /**
     * - text property binding
     */
    private void initGameFilterToggle() {
        // binding for dynamic label update
        StringBinding stringBinding = // automatically changing toggle text
                new When(gameFilterToggle.selectedProperty())
                        .then("Filtering off")
                        .otherwise("Filtering on");
        gameFilterToggle.textProperty().bind(stringBinding);
        // if turned off then reset regular game display TODO
    }

    /**
     * - bind disabled property
     * - bind content
     * - on selection change update gameHistoryListView
     */
    private void initTerminalTestComboBox() {
        // disable the terminalTestComboBox if the toggle is not selected
        terminalTestComboBox.disableProperty().bind(runsOrTestsToggle.selectedProperty().not());
        terminalTestComboBox.setItems(TERMINAL_TESTS_LIST);
        terminalTestComboBox
                .getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) ->
                {
                    if (newValue == null) return;
                    ObservableList<Game> games = FXCollections.observableArrayList();
                    games.addAll(newValue.getGameList());
                    gameHistoryListView.setItems(games);
                });
    }

    /**
     * - set initial content to single runs
     * - on item selection load game to display and collapse other titled panes
     */
    private void initGameHistoryListView() {
        // initial content
        gameHistoryListView.setItems(SINGLE_RUNS_LIST);

        List<TitledPane> toCollapse = Arrays.asList(loadProgressTitledPane, controlsTitledPane, gamesTitledPane);
        gameHistoryListView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) ->
                {
                    gameDisplay.setGame(newValue);
                    toCollapse.forEach(titledPane -> titledPane.setExpanded(false));
                });
    }

    /**
     * Creates a service to load runs from JSONs in the background
     * @return the service
     */
    private Service<Void> createRunLoaderService() {
        File[] runFilesToLoad = HistoryManager.getGameJSONFiles();
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() {
                        for (int i = 0; i < runFilesToLoad.length; i++) {
                            File file = runFilesToLoad[i];
                            final Game game = HistoryManager.loadSingleGameFromJSON(file);

                            // update UI components on FX Thread
                            final int finalI = i;
                            Platform.runLater(() ->
                            {
                                SINGLE_RUNS_LIST.add(game);
                                singleRunProgressLabel.setText((finalI + 1) + "/" + runFilesToLoad.length);
                            });
                        }
                        return null;
                    }
                };
            }
        };
        service.setOnSucceeded(e ->
        {
            // if the other service has finished as well then hide the pane
            if (terminalTestLoaderService.getState() == Worker.State.SUCCEEDED)
                loadProgressTitledPane.setExpanded(false);
            SINGLE_RUNS_LIST.sort(Comparator.comparingInt(Game::getID));
            gameHistoryListView.refresh();
        });
        return service;
    }

    /**
     * Creates a service to load TerminalTests from JSONs in the background
     * @return the service
     */
    private Service<Void> createTerminalTestLoaderService() {
        File[] testFilesToLoad = HistoryManager.getTerminalTestJSONFiles();
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() {
                        for (int i = 0; i < testFilesToLoad.length; i++) {
                            File file = testFilesToLoad[i];
                            final TerminalTest test = HistoryManager.loadSingleTerminalTestFromJSON(file);

                            // update UI components on FX Thread
                            final int finalI = i;
                            Platform.runLater(() ->
                            {
                                TERMINAL_TESTS_LIST.add(test);
                                terminalTestProgressLabel.setText((finalI + 1) + "/" + testFilesToLoad.length);
                            });
                        }
                        return null;
                    }
                };
            }
        };
        service.setOnSucceeded(e ->
        {
            // if the other service has finished as well then hide the pane
            if (singleRunsLoaderService.getState() == Worker.State.SUCCEEDED)
                loadProgressTitledPane.setExpanded(false);
            TERMINAL_TESTS_LIST.sort(Comparator.comparing(TerminalTest::getTestStart));
            terminalTestComboBox.setItems(TERMINAL_TESTS_LIST);
        });
        return service;
    }
}
