package com.ibjava.codingtomorrow.ui;

import com.ibjava.codingtomorrow.communication.NavigationSystem;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.communication.JSONProcessor;
import com.ibjava.codingtomorrow.controlclasses.Response;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.history.HistoryManager;
import com.ibjava.codingtomorrow.ui.display.TickDisplay;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.*;

/**
 * Controlling the car on run time
 */
public class PilotViewController implements Initializable {

    @FXML TitledPane keyboardLayoutPane;

    /**
     * Key map
     */
    private static final Map<KeyCode, String> KEYBOARD_LAYOUT = new HashMap<KeyCode, String>()
    {
        {
            /*Used ones*/
            put(KeyCode.ALT, Response.NO_OP);
            put(KeyCode.W, Response.ACCELERATION);
            put(KeyCode.S, Response.DECELERATION);
            put(KeyCode.A, Response.CAR_INDEX_LEFT);
            put(KeyCode.D, Response.CAR_INDEX_RIGHT);

            put(KeyCode.BACK_SPACE, Response.CLEAR);
            put(KeyCode.T, Response.FULL_THROTTLE);
            put(KeyCode.G, Response.EMERGENCY_BRAKE);
            put(KeyCode.F, Response.GO_LEFT);
            put(KeyCode.H, Response.GO_RIGHT);
        }
    };

    @FXML TitledPane tickDisplayPane;
    private TickDisplay tickDisplay = new TickDisplay(600);
    private RunOnPilot pilotService = new RunOnPilot();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        keyboardLayoutPane.setContent(createKeyboardLayoutDisplayGrid());
        tickDisplayPane.setContent(tickDisplay);
    }

    @FXML
    private void startGame(ActionEvent event) {
        if (pilotService.getState() == Worker.State.READY) {
            pilotService.start();
            System.out.println("pilotService started");
        } else {
            pilotService.restart();
            System.out.println("pilotService restarted");
        }
    }

    private class PilotTerminal extends Stage {

        // saving most recent command into this String
        private String command;

        public PilotTerminal() {
            super();
            StackPane root = new StackPane();
            ListView<String> listView = new ListView<>();
            root.getChildren().add(listView);
            Scene scene = new Scene(root);

            scene.setOnKeyPressed(event ->
            {
                command = KEYBOARD_LAYOUT.get(event.getCode());
                if (command != null) {
                    listView.getItems().add(command);
                } else {
                    System.out.println("No command assigned to " + event.getCode());
                }
            });

            setScene(scene);
        }

    }


    private class RunOnPilot extends Service<Void> {

        private NavigationSystem navi = NavigationSystem.getInstance();
        private Game game;
        private PilotTerminal terrminal;

        RunOnPilot() {
            game = new Game();
            terrminal = new PilotTerminal();
            setOnSucceeded(event -> saveRunHistory());
            setOnCancelled(event -> saveRunHistory());
            setOnFailed(event -> saveRunHistory());
        }

        @Override
        public void start() {
            terrminal.show();
            super.start();
        }

        @Override
        public void restart() {
            navi.restartNavigationSystem();
            terrminal.close();
            terrminal = new PilotTerminal();
            terrminal.show();
            super.restart();
        }

        /**
         * Writes the Game object to a JSON file and updates the history listview.
         */
        private void saveRunHistory() {
            HistoryManager.saveGameToHistory(game);
            HistoryViewController.SINGLE_RUNS_LIST.add(game);
            game = new Game();
        }

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    Tick tick = navi.startCommunication();
                    // as long as there is a connection to the server
                    while (navi.getClient().getClientSocket().isConnected()) {
                        game.getTicks().add(tick);
                        tickDisplay.setTick(tick);
                        final Tick t = tick;
                        Platform.runLater(() -> MainViewController.BOARD_UI.displayTick(t));
                        /*Read command from terminal in 2000 millis*/
                        Thread.sleep(2000);
                        Response resp = new Response(tick, terrminal.command);
                        tick = navi.respond(resp);
                    }
                    return null;
                }
            };
        }
    }

    /**
     * Creates a grid representing the KEYBOARD_LAYOUT map with Labels
     * @return
     */
    private GridPane createKeyboardLayoutDisplayGrid() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(5);
        int i = 0;
        List<Map.Entry<KeyCode, String>> entries = new ArrayList<>(KEYBOARD_LAYOUT.entrySet());
        entries.sort(Comparator.comparing(Map.Entry::getValue));
        for (Map.Entry<KeyCode, String> entry : entries) {
            Label commandLabel = new Label(entry.getValue());
            Label assignedKeyLabel = new Label(entry.getKey().toString());
            grid.addRow(i++, commandLabel, assignedKeyLabel);
        }
        return grid;
    }
}
