package com.ibjava.codingtomorrow.ui.board;

import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Entity;
import com.ibjava.codingtomorrow.entities.Train;
import com.ibjava.codingtomorrow.logic.analyser.MapAnalyser;
import com.ibjava.codingtomorrow.ui.MainViewController;
import com.ibjava.codingtomorrow.ui.UIMain;
import com.ibjava.codingtomorrow.ui.board.fields.BaseField;
import com.ibjava.codingtomorrow.ui.board.fields.BoardBaseField;
import com.ibjava.codingtomorrow.ui.board.fields.EntityBaseField;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.Collection;
import java.util.List;

/**
 * Represents a char matrix as a GridPane
 */
public class BoardUI extends GridPane {

    // the back-end matrix based on which the fields will be determined
    private char[][] charMatrix;

    /**
     * Calls the private constructor with a default starting cellSize of 50.
     * It will be always overridden, as the column and row constraints get bound to a spinner.
     * @param charMatrix the char matrix to represent.
     */
    public BoardUI(char[][] charMatrix) {
        this(charMatrix, 50);
    }

    private BoardUI(char[][] charMatrix, double cellSize) {
        super();
        setGridLinesVisible(true);
        this.charMatrix = charMatrix;

        // setting starting row size
        for (int r = 0; r < charMatrix.length; r++) {
            getRowConstraints().add(new RowConstraints(cellSize, cellSize, cellSize, Priority.NEVER, VPos.CENTER, true));
        }

        // setting starting column size
        for (int c = 0; c < charMatrix[0].length; c++) {
            getColumnConstraints().add(new ColumnConstraints(cellSize, cellSize, cellSize, Priority.NEVER, HPos.CENTER, true));
        }

        // populating the grid with fields
        for (int r = 0; r < charMatrix.length; r++) {
            for (int c = 0; c < charMatrix[0].length; c++) {
                // the character to be represented
                char ch = charMatrix[r][c];

                // The field will always fit to the column and row constraints width and height
                DoubleProperty widthBean = getColumnConstraints().get(c).prefWidthProperty();
                DoubleProperty heightBean = getRowConstraints().get(r).prefHeightProperty();
                BoardBaseField field = new BoardBaseField(ch, widthBean, heightBean);

                /* Show coordinates on hover */
                // saving all previously set actions to this event
                EventHandler<? super MouseEvent> onEntered = field.getOnMouseEntered();
                field.setOnMouseEntered(e ->
                {
                    if (onEntered != null) onEntered.handle(e);
                    MainViewController.hoveredRow.set(GridPane.getRowIndex(field));
                    MainViewController.hoveredCol.set(GridPane.getColumnIndex(field));
                });

                /*Allow position selection by click*/
                int X = c;
                int Y = r;

                // start selection with left click
                // saving all previously set actions to this event
                EventHandler<? super MouseEvent> onClicked = field.getOnMouseClicked();
                field.setOnMouseClicked(e ->
                {
                    if (onClicked != null) onClicked.handle(e);
                    UIMain.mainViewController.getStartPosTextField().setText(X + ";" + Y);
                });

                // end selection with right click
                // saving all previously set actions to this event
                EventHandler<? super ContextMenuEvent> onContextMenuReq = field.getOnContextMenuRequested();
                field.setOnContextMenuRequested(e ->
                {
                    if (onContextMenuReq != null) onContextMenuReq.handle(e);
                    UIMain.mainViewController.getEndPosTextField().setText(X + ";" + Y);
                });

                // adding the created and customized field to the grid
                add(field, c, r);
            }
        }
    }

    /**
     * Returns the BaseField object based on its position
     * @param pos the desired position of the BaseField
     * @return
     */
    private BaseField getField(Position pos) {
        byte r = pos.getY();
        byte c = pos.getX();
        return (BaseField)getChildren()
                        .stream()
                        .filter(node -> GridPane.getRowIndex(node) == r && GridPane.getColumnIndex(node) == c)
                        .filter(node -> node instanceof BaseField)
                        .toArray(Node[]::new)[0];
    }

    /**
     * Highlights an array of BaseField objects
     * @param positions the positions on which BaseField objects will be highlighted
     */
    public void highlightFields(Position... positions) {
        for (int i = 0; i < positions.length; i++) {
            Position pos = positions[i];
            BaseField field = getField(pos);

            // mark startPosition if first
            if (i == 0) field.markAsStartPosition();

            //mark endPosition if last
            if (i == positions.length - 1) field.markAsEndPosition();

            field.getHighlighter().setVisible(true);
        }
    }

    /**
     * Sets the visible property of each BaseField object.
     * @param b
     */
    public void showLabels(boolean b) {
        getChildren()
            .stream()
            .filter(node -> node instanceof BaseField)
            .forEach(node -> ((BaseField)node).getLabel().setVisible(b));
    }

    /**
     * Turns the highlight off for each BaseField object
     */
    public void allHighlightOff() {
        getChildren()
                .stream()
                .filter(node -> node instanceof BaseField)
                .forEach(node ->
                {
                    BaseField field = (BaseField)node;
                    field.getHighlighter().setVisible(false);
                    field.removeMarks();
                });
    }

    /**
     * Places an entity on the board
     * @param entity the entity to be placed
     */
    private void placeEntity(Entity entity) {
        // first creating an EntityBaseField object to represent the Entity
        // The field will always fit to the column and row constraints width and height
        DoubleBinding radiusBean = getColumnConstraints().get(0).prefWidthProperty().divide(2);
        EntityBaseField entityField = new EntityBaseField(entity, radiusBean);

        // hard code to identify own car
        if (entity.getPos() == OUR_CAR.getPos()) {
            if (OUR_CAR.hasPassenger()) {
                entityField.getFieldShape().setFill(ShapeFactory.FillManager.OUR_HAS_PASSENGER_COLOR);
            } else {
                entityField.getFieldShape().setFill(ShapeFactory.FillManager.OUR_CAR_COLOR);
            }
        }

        if (entity.getPos().isValid()) add(entityField, entity.getPos().getX(), entity.getPos().getY());
    }

    private void placeTrain(Train train, Tick tick) {
        List<Position> positions = train.getAllOccupiedPositions(tick.getRequest_id().getTick());
        DoubleProperty widthBean = getColumnConstraints().get(0).prefWidthProperty();
        DoubleProperty heightBean = getRowConstraints().get(0).prefHeightProperty();

        Position head = train.getHead(tick.getRequest_id().getTick());
        Position tail = train.getTail(tick.getRequest_id().getTick());
        positions.forEach(pos ->
        {
            Paint fill = ShapeFactory.FillManager.TRAIN_COLOR;
            if (head != null) {
                if (pos.equals(head)) {
                    fill = ShapeFactory.FillManager.TRAIN_HEAD_COLOR;
                }
            }
            if (tail != null) {
                if (pos.equals(tail)) {
                    fill = ShapeFactory.FillManager.TRAIN_TAIL_COLOR;
                }
            }
            add(ShapeFactory.createTrainWagon(widthBean, heightBean, fill), pos.getX(), pos.getY());
        });
    }

    private void placeTrains(Collection<? extends Train> trains, Tick tick) {
        trains.forEach(t -> placeTrain(t, tick));
    }

    /**
     * Places entities on the board
     * @param entities an array of entities to be placed on the board
     */
    private void placeEntities(Entity... entities) {
        for (Entity e : entities) {
            placeEntity(e);
        }
    }

    private static Car OUR_CAR = null;

    /**
     * A complete tick gets displayed by placing all entities of the Tick to the board.
     * @param tick
     */
    public void displayTick(Tick tick) {
        OUR_CAR = tick.getOurCar();
        clearEntityBaseFields();
        placeEntities(tick.getCars());
        placeEntities(tick.getPassengers());
        placeEntities(tick.getPedestrians());

        clearTrainWagons();
        placeTrains(MapAnalyser.TRAINS, tick);
    }

    /**
     * Removes all EntityBaseField objects from the grid
     */
    private void clearEntityBaseFields() {
        Node[] toRemove =
                getChildren()
                .stream()
                .filter(node -> node instanceof EntityBaseField)
                .toArray(Node[]::new);
        getChildren().removeAll(toRemove);
    }

    private void clearTrainWagons() {
        Node[] toRemove =
                getChildren()
                        .stream()
                        .filter(node -> node instanceof Rectangle)
                        .filter(node -> ((Rectangle) node).getArcWidth() > 0)
                        .toArray(Node[]::new);
        getChildren().removeAll(toRemove);
    }

}
