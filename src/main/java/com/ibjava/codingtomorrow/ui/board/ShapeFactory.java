package com.ibjava.codingtomorrow.ui.board;

import com.ibjava.codingtomorrow.entities.*;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;
import java.util.Map;

/**
 * This class generates Shapes to represents given fields and entities on the board
 */
public class ShapeFactory {

    /**
     * Returns a rectangle representing a basic board field from a predefined set of characters.
     * @param ch S, Z, P, G, B, T
     * @param widthBean the widthProperty to which the width of this Rectangle will be bound
     * @param heightBean the heightProperty to which the height of this Rectangle will be bound
     * @return the constructed Rectangle
     */
    public static Rectangle createBoardField(Character ch, DoubleProperty widthBean, DoubleProperty heightBean) {
        Rectangle rect = new Rectangle();
        rect.widthProperty().bind(widthBean);
        rect.heightProperty().bind(heightBean);
        rect.setFill(FillManager.CHARACTER_FILL_MAP.get(ch));
        return rect;
    }

    /**
     * Returns a rectangle used to highlight fields on specific actions
     * @param widthBean the widthProperty to which the width of this Rectangle will be bound
     * @param heightBean the heightProperty to which the height of this Rectangle will be bound
     * @return the constructed Rectangle
     */
    public static Rectangle createFieldHighlighter(ReadOnlyDoubleProperty widthBean, ReadOnlyDoubleProperty heightBean) {
        Rectangle rect = new Rectangle();
        rect.widthProperty().bind(widthBean);
        rect.heightProperty().bind(heightBean);
        rect.setFill(FillManager.HIGHLIGHT_COLOR);
        return rect;
    }

    /**
     * Returns a circle representing the Entity parameter.
     * @param entity the Entity to be represented.
     * @param radiusBean the property to which the radius of this Circle will be bound
     * @return the constructed Circle
     */
    public static Circle createEntityField(Entity entity, DoubleBinding radiusBean) {
        return createEntityField(entity.getClass(), radiusBean);
    }

    /**
     * Returns a circle representing an instance of the Entity class.
     * Based on which child class is being represented the circle's color is changed.
     * @param entityClass a child class of the Entity abstract class which will be represented
     * @param radiusBean the property to which the radius of this Circle will be bound
     * @return the constructed Circle
     */
    public static Circle createEntityField(Class<? extends Entity> entityClass, DoubleBinding radiusBean) {
        Circle circle = new Circle();
        circle.radiusProperty().bind(radiusBean);
        circle.setFill(FillManager.ENTITY_FILL_MAP.get(entityClass));
        return circle;
    }

    /**
     * Returns a circle representing an instance of the Entity class.
     * The circle's color is overridden in the parameters.
     * @param entityClass a child class of the Entity abstract class which will be represented
     * @param radiusBean the property to which the radius of this Circle will be bound
     * @return the constructed Circle
     */
    public static Circle createEntityField(Class<? extends Entity> entityClass, DoubleBinding radiusBean, Paint fill) {
        Circle circle = createEntityField(entityClass, radiusBean);
        circle.setFill(fill);
        return circle;
    }

    public static Rectangle createTrainWagon(ReadOnlyDoubleProperty widthBean, ReadOnlyDoubleProperty heightBean, Paint fill) {
        Rectangle rectangle = new Rectangle();
        rectangle.widthProperty().bind(widthBean.multiply(0.9));
        rectangle.heightProperty().bind(heightBean.multiply(0.9));
        rectangle.setFill(fill);
        rectangle.arcHeightProperty().bind(widthBean.multiply(0.5));
        rectangle.arcWidthProperty().bind(widthBean.multiply(0.5));
        rectangle.strokeWidthProperty().bind(widthBean.multiply(0.1));
        rectangle.setStroke(Color.WHITE);
        return rectangle;
    }

    /**
     * Manages assigning color to the above constructed shapes
     */
    public static class FillManager {

        /**
         * Assigns colors to Entity types
         */
        private static final Map<Class<? extends Entity>, Paint> ENTITY_FILL_MAP = new HashMap<Class<? extends Entity>, Paint>()
        {
            {
                put(Car.class, Color.BLUE);
                put(Passenger.class, Color.LIME);
                put(Pedestrian.class, Color.DEEPPINK);
            }
        };

        /**
         * Assigns colors to characters
         */
        private static final Map<Character, Paint> CHARACTER_FILL_MAP = new HashMap<Character, Paint>()
        {
            {
                put('S', Color.GRAY);
                put('Z', Color.HONEYDEW);
                put('P', Color.CADETBLUE);
                put('G', Color.FORESTGREEN);
                put('B', Color.DARKMAGENTA);
                put('T', Color.SADDLEBROWN);
                put('R', Color.BLACK);
                put('C', Color.BURLYWOOD);
                put('X', Color.LIGHTSTEELBLUE);
            }
        };

        /**
         * Color used to highlight fields
         */
        private static final Paint HIGHLIGHT_COLOR = Color.rgb(240, 50, 0, 0.5);

        /**
         * Color used to identify OUR_CAR car
         */
        public static final Paint OUR_CAR_COLOR = Color.web("#FAD02C");

        /**
         * Color used to indicate that OUR_CAR car has a passenger
         */
        public static final Paint OUR_HAS_PASSENGER_COLOR = Color.web("#AB0552");

        public static final Paint TRAIN_COLOR = Color.RED;
        public static final Paint TRAIN_HEAD_COLOR = Color.YELLOWGREEN;
        public static final Paint TRAIN_TAIL_COLOR = Color.DARKORCHID;

    }

}
