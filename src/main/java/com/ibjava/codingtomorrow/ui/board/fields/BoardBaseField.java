package com.ibjava.codingtomorrow.ui.board.fields;

import com.ibjava.codingtomorrow.ui.board.ShapeFactory;
import javafx.beans.property.DoubleProperty;
import javafx.scene.shape.Rectangle;

/**
 * Template for a base field for the BoardUI.
 * It represents a character with a specific color and its width can be bound to grid constraint properties.
 */
public class BoardBaseField extends BaseField {

    public BoardBaseField(char ch, DoubleProperty widthBean, DoubleProperty heightBean) {
        super();
        fieldShape = ShapeFactory.createBoardField(ch, widthBean, heightBean);

        label.setText(String.valueOf(ch));

        prefWidthProperty().bind(widthBean);
        maxWidthProperty().bind(widthBean);
        minWidthProperty().bind(widthBean);

        prefHeightProperty().bind(heightBean);
        maxHeightProperty().bind(heightBean);
        minHeightProperty().bind(heightBean);

        getChildren().addAll(fieldShape, label, highlighter);
    }

    @Override
    public Rectangle getFieldShape() {
        return (Rectangle) super.getFieldShape();
    }
}
