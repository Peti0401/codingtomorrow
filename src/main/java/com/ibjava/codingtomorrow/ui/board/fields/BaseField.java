package com.ibjava.codingtomorrow.ui.board.fields;

import com.ibjava.codingtomorrow.ui.board.ShapeFactory;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * A base class for fields which are represented by a colored Rectangle and can be labeled and highlighted.
 */
public abstract class BaseField extends StackPane {

    Shape fieldShape;
    Label label;
    Rectangle highlighter;

    public BaseField() {
        super();
        getStyleClass().add("field");

        label = new Label();
        label.setVisible(false);

        highlighter = ShapeFactory.createFieldHighlighter(widthProperty(), heightProperty());
        highlighter.setVisible(false);

        // instead of CSS hover pseudo class which slows the UI
        setOnMouseEntered(e -> highlighter.setVisible(true));
        setOnMouseExited(e -> highlighter.setVisible(false));
    }

    public Shape getFieldShape() {
        return fieldShape;
    }

    public Label getLabel() {
        return label;
    }

    public Rectangle getHighlighter() {
        return highlighter;
    }

    public void markAsStartPosition() {
        MaterialDesignIconView icon = new MaterialDesignIconView(MaterialDesignIcon.PIN);
        getChildren().add(icon);
    }

    public void markAsEndPosition() {
        MaterialDesignIconView icon = new MaterialDesignIconView(MaterialDesignIcon.FLAG_CHECKERED);
        getChildren().add(icon);
    }

    public void removeMarks() {
        getChildren().removeIf(node -> node instanceof MaterialDesignIconView);
    }
}
