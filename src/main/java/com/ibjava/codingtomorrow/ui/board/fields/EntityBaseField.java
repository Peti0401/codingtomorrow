package com.ibjava.codingtomorrow.ui.board.fields;

import com.ibjava.codingtomorrow.Utils;
import com.ibjava.codingtomorrow.entities.Entity;
import com.ibjava.codingtomorrow.ui.board.ShapeFactory;
import com.sun.deploy.util.ReflectionUtil;
import javafx.beans.binding.DoubleBinding;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Template for a field representing entities on the BoardUI.
 */
public class EntityBaseField extends BaseField {

    // the entity to be represented
    private Entity entity;

    public EntityBaseField(Entity entity, DoubleBinding radiusBean) {
        super();
        this.entity = entity;
        fieldShape = ShapeFactory.createEntityField(entity, radiusBean);

        highlighter.widthProperty().bind(radiusBean.multiply(2));
        highlighter.heightProperty().bind(radiusBean.multiply(2));

        setOnMouseClicked(e -> openEntityInspector());

        getChildren().addAll(fieldShape, label, highlighter);
    }

    /**
     * Used as a run-time debug tool
     */
    private void openEntityInspector() {
        Stage stage = new Stage();
        stage.setTitle("Inspector of " + entity.getClass().getSimpleName() + " #" + entity.getId());
        Scene scene = new Scene(createEntityInspectorFrame());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Creates a StackPane which wraps a TextArea in which this Entity is described with all its fields.
     * @return
     */
    private StackPane createEntityInspectorFrame() {
        StackPane frame = new StackPane();
        List<Field> fields = Utils.ReflectionUtil.getFields(entity);
        String[] entityInfoArr =
                fields
                .stream()
                .map(field -> field.getType().getSimpleName() + " " + field.getName() + ": " + Utils.ReflectionUtil.callGetter(field, entity))
                .toArray(String[]::new);

        String entityInfo = String.join("\n\n", entityInfoArr);
        TextArea ta = new TextArea(entityInfo);
        ta.setPrefHeight(400);
        ta.setEditable(false);
        ta.setWrapText(true);
        frame.getChildren().add(ta);

        return frame;
    }

    public Entity getEntity() {
        return entity;
    }


}
