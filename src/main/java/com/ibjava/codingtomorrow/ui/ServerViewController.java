package com.ibjava.codingtomorrow.ui;

import com.ibjava.codingtomorrow.communication.NavigationSystem;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.controlclasses.Response;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.history.HistoryManager;
import com.ibjava.codingtomorrow.ui.display.TickDisplay;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Communication with the server gets displayed on the UI on run-time
 */
public class ServerViewController implements Initializable {

    @FXML TitledPane tickDisplayPane;
    @FXML ListView<String> commandsListView;
    private RunOnServer serverService = new RunOnServer();
    private TickDisplay tickDisplay = new TickDisplay(600);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tickDisplayPane.setContent(tickDisplay);
    }

    @FXML
    private void startGame(ActionEvent event) {
        MainViewController.BOARD_UI.allHighlightOff();
        if (serverService.getState() == Worker.State.READY) {
            serverService.start();
            System.out.println("serverService started");
        } else {
            serverService.restart();
            System.out.println("serverService restarted");
        }
    }

    /**
     * Service to simultaneously manage background communication with the server and game displaying on the UI
     */
    private class RunOnServer extends Service<Void> {

        private Game game; // used to save ticks and then write to JSON
        private NavigationSystem navi = NavigationSystem.getInstance();

        RunOnServer() {
            game = new Game();
            // in case of each termination state, the JSON is saved
            setOnSucceeded(event -> saveRunHistory());
            setOnCancelled(event -> saveRunHistory());
            setOnFailed(event -> saveRunHistory());
        }

        @Override
        public void restart() {
            navi.restartNavigationSystem();
            super.restart();
        }

        /**
         * Writes the Game object to a JSON file and updates the history list view.
         */
        private void saveRunHistory() {
            if (!game.getTicks().isEmpty()) {
                HistoryManager.saveGameToHistory(game);
                HistoryViewController.SINGLE_RUNS_LIST.add(game);
            }
            commandsListView.getItems().addAll(game.getCommands());
            game = new Game();
        }

        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    Tick tick = navi.startCommunication();
                    // as long as there is a connection to the server
                    while (navi.getClient().getClientSocket().isConnected()) {
                        /* UI UPDATES */
                        tickDisplay.setTick(tick); // display the tick's JSON / toString()
                        final Tick t = tick; // effectively final variable extracted for lambda
                        Platform.runLater(() -> MainViewController.BOARD_UI.displayTick(t)); // display the tick on the board

                        /* SERVER UPDATES */
                        Response resp = NavigationSystem.navigate(tick);
                        game.add(tick, resp); // save this tick and the response to it
                        tick = navi.respond(resp);
                        Thread.sleep(25); // to balance UI updates
                    }
                    return null;
                }
            };
        }
    }

}
