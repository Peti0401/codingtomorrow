package com.ibjava.codingtomorrow.ui.display;

import com.ibjava.codingtomorrow.Utils;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.ui.board.BoardUI;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * Wraps a TickDisplay and a command list view into one, with animated game review integration
 */
public class GameDisplay extends VBox {

    private Game game; // the game which gets displayed
    private BoardUI boardUI; // the Board on which the animation happens
    private ListView<String> commandsListView; // commands of the game
    private TickDisplay tickDisplay; // tick display

    private Timeline timeline = new Timeline(); // timeline to interpolate tick selection
    private JFXTextField millisTextField; // input for millis / tick
    private Label timeLabel; // label to display full time of the animation ( [numTicks * millis] / 1000 seconds )
    private Tooltip tooltip; // tooltip for game info

    public GameDisplay(BoardUI boardUI) {
        super(10);
        setAlignment(Pos.TOP_CENTER);

        this.boardUI = boardUI;
        commandsListView = new ListView<>();
        tickDisplay = new TickDisplay();

        HBox containerBox = new HBox(); // wraps the command list view and the tick display horizontally
        containerBox.setAlignment(Pos.TOP_CENTER);
        containerBox.getChildren().addAll(commandsListView, tickDisplay);

        HBox controlsBox = new HBox(10); // wraps control buttons horizontally
        controlsBox.setAlignment(Pos.CENTER);

        timeLabel = new Label("Run time:");
        millisTextField = new JFXTextField("75");
        millisTextField.setPrefWidth(100);
        millisTextField.setAlignment(Pos.CENTER);
        // on text change recalculate full run time
        millisTextField
                .textProperty()
                .addListener((observable, oldValue, newValue) ->
                {
                    if (newValue.isEmpty()) return;
                    try {
                        double millis = Double.parseDouble(newValue);
                        double duration = game.getTicks().size() * millis;
                        double seconds = duration / 1000;
                        timeLabel.setText("Run time: " + seconds + " seconds");
                    } catch (Exception e) { // parsing error
                        Utils.AlertUtil.alert("Hey", "Please don't be dumb.\nThank you.", Alert.AlertType.ERROR);
                    }
                });

        millisTextField.setPromptText("Millis per ticks");
        Button playButton = new Button("Play");

        // using a timeline the selected index property of the commands list view will be interpolated
        playButton.setOnAction(e ->
        {
            // if pressed while playing, it is reset
            switch (timeline.getStatus()) {
                case RUNNING:
                    stopTimeline();
                    break;
                case PAUSED:
                    timeline.play();
                    return;
            }
            initTimeline();
            timeline.playFromStart();
        });

        Button pauseButton = new Button("Pause");
        pauseButton.setOnAction(e -> {if (timeline.getStatus() == Animation.Status.RUNNING) timeline.pause(); });

        Button stopButton = new Button("Stop");
        stopButton.setOnAction(e -> stopTimeline());

        controlsBox.getChildren().addAll(millisTextField, timeLabel, playButton, pauseButton, stopButton);

        getChildren().addAll(controlsBox, containerBox);

        // install info tooltip
        tooltip = new Tooltip("Choose a game from the list to display!");
        tooltip.setWrapText(true);
        Tooltip.install(this, tooltip);
    }

    private void updateCommandsListView() {
        commandsListView.getItems().setAll(game.getGameDescription());
        // when a new command is selected then display the corresponding tick
        commandsListView.getSelectionModel()
                .selectedIndexProperty()
                .addListener((observable, oldValue, newValue) ->
                {
                    // indices are synchronized
                    final Tick tickToDisplay = game.getTicks().get(newValue.intValue());
                    tickDisplay.setTick(tickToDisplay); // display tick text content
                    boardUI.displayTick(tickToDisplay); // display tick on board
                    commandsListView.scrollTo(newValue.intValue()); // scroll to new value
                });
    }

    private void initTimeline() {
        timeline.getKeyFrames().clear();
        double millis = Double.parseDouble(millisTextField.getText());
        KeyFrame keyFrame = new KeyFrame
                (
                        Duration.millis(millis), // duration of a single tick change
                        event -> // this happens in every cycle
                        {
                            int curSelectedIndex = commandsListView.getSelectionModel().getSelectedIndex();
                            if (curSelectedIndex < commandsListView.getItems().size()) { // check index out of bounds
                                int newIndex = curSelectedIndex + 1;
                                commandsListView.getSelectionModel().select(newIndex); // interpolate
                                commandsListView.scrollTo(newIndex); // scroll
                            }
                        }
                );
        timeline.getKeyFrames().add(keyFrame);

        // play the keyframe this many times
        int numTicks = game.getTicks().size();
        timeline.setCycleCount(numTicks);

        // reset on finish
        timeline.setOnFinished(finished -> commandsListView.getSelectionModel().select(0));
    }

    /**
     * Stops the timeline and jumps back to 0 command index
     */
    private void stopTimeline() {
        timeline.stop();
        commandsListView.getSelectionModel().select(0); // jump to 0 index
    }

    /**
     * Sets the Game to display. If a previous Game was in animation then it is stopped.
     * @param game new value of game
     */
    public void setGame(Game game) {
        if (timeline.getStatus() == Animation.Status.RUNNING) timeline.stop();
        this.game = game;
        updateCommandsListView();
        tooltip.setText("Displaying:\n" + game);
    }

}
