package com.ibjava.codingtomorrow.ui.display;

import com.ibjava.codingtomorrow.communication.JSONProcessor;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.jfoenix.controls.JFXToggleButton;
import javafx.beans.binding.StringBinding;
import javafx.beans.binding.When;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;


/**
 * A component wrapping a text area and a toggle.
 * Aim is to easily change between the toString() and JSON representation of a Tick.
 */
public class TickDisplay extends VBox {

    private Tick tick;
    private TextArea textArea;
    private JFXToggleButton toggleButton;

    public TickDisplay() {
        this(600);
    }

    public TickDisplay(double textAreaPrefHeight) {
        super();

        textArea = new TextArea();
        textArea.setEditable(false);

        textArea.setPrefWidth(250);
        textArea.setPrefHeight(textAreaPrefHeight);

        toggleButton = new JFXToggleButton();
        toggleButton.selectedProperty().addListener((observable, oldValue, newValue) ->
        {
            if (tick == null) return;
            if (newValue) { // on == JSON
                textArea.setText(JSONProcessor.tickToJSON(tick));
            } else { // off == toString()
                textArea.setText(tick.toString());
            }
        });

        StringBinding tickInfoFormatToggleLabelBinding = // automatically changing toggle text
                new When(toggleButton.selectedProperty())
                        .then("toString()")
                        .otherwise("JSON");
        toggleButton.textProperty().bind(tickInfoFormatToggleLabelBinding);

        getChildren().addAll(toggleButton, textArea);
    }

    public Tick getTick() {
        return tick;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public JFXToggleButton getToggleButton() {
        return toggleButton;
    }

    public void setTick(Tick tick) {
        this.tick = tick;
        if (toggleButton.isSelected()) { // on == JSON
            textArea.setText(JSONProcessor.tickToJSON(tick));
        } else { // off == toString()
            textArea.setText(tick.toString());
        }
    }
}
