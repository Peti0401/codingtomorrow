package com.ibjava.codingtomorrow.ui;

import com.ibjava.codingtomorrow.Utils;
import com.ibjava.codingtomorrow.controlclasses.Board;
import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.entities.Pedestrian;
import com.ibjava.codingtomorrow.logic.navigation.PathFinder;
import com.ibjava.codingtomorrow.ui.board.BoardUI;
import com.ibjava.codingtomorrow.ui.board.ShapeFactory;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.binding.When;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;

public class MainViewController implements Initializable {

    @FXML SplitPane root;
    @FXML ScrollPane scrollPane;

    /*BOARD DEBUG TAB*/
    //Legend
    @FXML GridPane boardFieldsGrid;
    @FXML GridPane entityFieldsGrid;
    //Board parameters
    @FXML JFXToggleButton gridLinesToggle;
    @FXML JFXToggleButton charactersToggle;
    @FXML Label activeFieldLabel;
    public static SimpleIntegerProperty hoveredCol = new SimpleIntegerProperty(0);
    public static SimpleIntegerProperty hoveredRow = new SimpleIntegerProperty(0);
    //Navigation system
    @FXML JFXTextField startPosTextField;
    @FXML JFXTextField endPosTextField;

    /*SERVER TAB*/
    @FXML Tab serverTab;
    private static final String SERVER_VIEW_FXML_PATH = "ServerView.fxml";
    static ServerViewController serverViewController;

    /*PILOT TAB*/
    @FXML Tab pilotTab;
    private static final String PILOT_VIEW_FXML_PATH = "PilotView.fxml";
    static PilotViewController pilotViewController;

    /*HISTORY TAB*/
    @FXML Tab historyTab;
    private static final String HISTORY_VIEW_FXML_PATH = "HistoryView.fxml";
    static HistoryViewController historyViewController;

    public static final BoardUI BOARD_UI = new BoardUI(Board.getInstance().getCharMatrix());

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Pair<Node, Initializable> serverViewPair = loadFXML(SERVER_VIEW_FXML_PATH);
        serverTab.setContent(serverViewPair.getKey());
        serverViewController = (ServerViewController)serverViewPair.getValue();

        Pair<Node, Initializable> pilotViewPair = loadFXML(PILOT_VIEW_FXML_PATH);
        pilotTab.setContent(pilotViewPair.getKey());
        pilotViewController = (PilotViewController) pilotViewPair.getValue();

        Pair<Node, Initializable> historyViewPair = loadFXML(HISTORY_VIEW_FXML_PATH);
        historyTab.setContent(historyViewPair.getKey());
        historyViewController = (HistoryViewController) historyViewPair.getValue();

        // place the BOARD_UI to the center
        scrollPane.setContent(BOARD_UI);

        initLegend();
        bindBoardFields();
    }

    private void bindBoardFields() {
        // binding cell size to spinner value
        BOARD_UI.getRowConstraints().forEach(r ->
        {
            final int numRows = BOARD_UI.getRowConstraints().size();
            DoubleBinding heightBinding = scrollPane.widthProperty().subtract(10).divide(numRows);
            r.prefHeightProperty().bind(heightBinding);
            r.minHeightProperty().bind(heightBinding);
            r.maxHeightProperty().bind(heightBinding);
        });
        BOARD_UI.getColumnConstraints().forEach(r ->
        {
            final int numCols = BOARD_UI.getColumnConstraints().size();
            DoubleBinding widthBinding = scrollPane.widthProperty().subtract(10).divide(numCols);
            r.prefWidthProperty().bind(widthBinding);
            r.minWidthProperty().bind(widthBinding);
            r.maxWidthProperty().bind(widthBinding);
        });

        // binding grid lines to toggle
        BOARD_UI.gridLinesVisibleProperty().bind(gridLinesToggle.selectedProperty());
        StringBinding gridToggleLabelBinding = // automatically changing toggle text
                new When(gridLinesToggle.selectedProperty())
                .then("Off")
                .otherwise("On");
        gridLinesToggle.textProperty().bind(gridToggleLabelBinding);

        // setting up view mode switching
        charactersToggle.setOnAction(e -> BOARD_UI.showLabels(charactersToggle.isSelected()));
        StringBinding charactersToggleLabelBinding = // automatically changing toggle text
                new When(charactersToggle.selectedProperty())
                        .then("Off")
                        .otherwise("On");
        charactersToggle.textProperty().bind(charactersToggleLabelBinding);

        // on mouse hover event update label
        activeFieldLabel
                .textProperty()
                .bind(Bindings.concat("[x=").concat(hoveredCol).concat(";").concat("y=").concat(hoveredRow).concat("]"));
    }

    /**
     * Highlights the algorithmically calculated path between two positions
     * @param event
     */
    @FXML
    private void highlightPath(ActionEvent event) {
        try {
            // making all previous highlight disappear
            BOARD_UI.allHighlightOff();
            String[] startPosString = startPosTextField.getText().split(";");
            String[] endPosString = endPosTextField.getText().split(";");
            Position startPos = new Position(Byte.valueOf(startPosString[0]), Byte.valueOf(startPosString[1]));
            Position endPos = new Position(Byte.valueOf(endPosString[0]), Byte.valueOf(endPosString[1]));

            Position[] path = PathFinder.getPath_escape(startPos, endPos, new Passenger[0], Direction.values());
            BOARD_UI.highlightFields(path);

            // Navigation related debug should come below...
        } catch (Exception e) {
            Utils.AlertUtil.alert("Wrong format", "The proper format is x;y", Alert.AlertType.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * Switches the previous highlight off
     * @param event
     */
    @FXML
    private void highlightOff(ActionEvent event) {
        BOARD_UI.allHighlightOff();
    }

    /**
     * Legend in which the colors and shapes assigned to entities is clarified
     */
    private void initLegend() {
        DoubleProperty widthBean = boardFieldsGrid.getColumnConstraints().get(0).prefWidthProperty();
        DoubleProperty heightBean = boardFieldsGrid.getRowConstraints().get(0).prefHeightProperty();

        /* BoardField objects */
        boardFieldsGrid.add(ShapeFactory.createBoardField('S', widthBean, heightBean), 1, 0);
        boardFieldsGrid.add(ShapeFactory.createBoardField('Z', widthBean, heightBean), 1, 1);
        boardFieldsGrid.add(ShapeFactory.createBoardField('P', widthBean, heightBean), 1, 2);
        boardFieldsGrid.add(ShapeFactory.createBoardField('B', widthBean, heightBean), 1, 3);
        boardFieldsGrid.add(ShapeFactory.createBoardField('T', widthBean, heightBean), 1, 4);
        boardFieldsGrid.add(ShapeFactory.createBoardField('R', widthBean, heightBean), 1, 5);
        boardFieldsGrid.add(ShapeFactory.createBoardField('C', widthBean, heightBean), 1, 6);
        boardFieldsGrid.add(ShapeFactory.createBoardField('X', widthBean, heightBean), 1, 7);

        /* EntityField objects */
        DoubleBinding radiusBean = boardFieldsGrid.getRowConstraints().get(0).prefHeightProperty().divide(2);
        entityFieldsGrid.add(ShapeFactory.createEntityField(Car.class, radiusBean), 1, 0);
        entityFieldsGrid.add(ShapeFactory.createEntityField(Car.class, radiusBean, ShapeFactory.FillManager.OUR_CAR_COLOR), 1, 1);
        entityFieldsGrid.add(ShapeFactory.createEntityField(Car.class, radiusBean, ShapeFactory.FillManager.OUR_HAS_PASSENGER_COLOR), 1, 2);
        entityFieldsGrid.add(ShapeFactory.createEntityField(Passenger.class, radiusBean), 1, 3);
        entityFieldsGrid.add(ShapeFactory.createEntityField(Pedestrian.class, radiusBean), 1, 4);
    }

    public JFXTextField getStartPosTextField() {
        return startPosTextField;
    }

    public JFXTextField getEndPosTextField() {
        return endPosTextField;
    }

    private static Pair<Node, Initializable> loadFXML(String FXMLPath) {
        FXMLLoader loader = new FXMLLoader(MainViewController.class.getResource(FXMLPath));
        try {
            Node node = loader.load();
            Initializable controller = loader.getController();
            return new Pair<>(node, controller);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
