package com.ibjava.codingtomorrow.communication;

import java.io.*;
import java.net.Socket;

public class Client {

    private final static String SERVER_IP = "10.100.1.150";
    private final static int PORT = 12323;

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    /**
     * connection gets established to the specified IP and port number.
     * @param ip
     * @param port
     */
    private void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            // out writer to write message to the server
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            // in reader to read the response of the server
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            System.out.println("Error at starting connection. " + e);
        }
    }

    /**
     * Connection gets established using the predefined final IP and port variables.
     */
    void startConnection() {
        startConnection(SERVER_IP, PORT);
        System.out.println("Connected to " + SERVER_IP + ":" + PORT);
    }

    /**
     * Stops the connection by closing the writer, the reader and the socket
     */
    public void stopConnection() {
        try {
            in.close();
            out.close();
            clientSocket.close();
            System.out.println("Connection with " + SERVER_IP + ":" + PORT + " stopped");
        } catch (Exception e) {
            System.out.println("Stopping connection failed. " + e);
        }
    }

    public boolean isConnected() {
        return !clientSocket.isClosed();
    }

    /**
     * Sends a message to the server and returns the server's response to it as a String
     * @param msg
     * @return
     */
    String sendMessage(String msg) {
        try {
            // writing the message to the server
            out.println(msg);
            String serverResponse = "";
            // counting braces the end the line at the last closing one
            int braces = 0;
            do {
                // casting int to char
                char ch = (char)in.read();
                if (ch == '{') {
                    braces++;
                } else if (ch == '}'){
                    braces--;
                }
                serverResponse += ch;
            } while (braces > 0);
            return serverResponse;
        } catch (Exception e) {
            System.out.println("Error at sending message. " + e);
            return null;
        }
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}
