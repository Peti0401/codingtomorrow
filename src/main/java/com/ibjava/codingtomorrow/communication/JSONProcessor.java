package com.ibjava.codingtomorrow.communication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.controlclasses.Response;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.test.TerminalTest;

import java.io.*;

/**
 * Manages processing JSON Strings into objects and vice-versa
 */
public class JSONProcessor {

    /**
     * The single object which is used for converting purpose
     */
    private static final Gson PRETTY_GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final Gson REGULAR_GSON = new Gson();

    /**
     * Returns a Tick object from a JSON String
     * @param JSON the String representation of a Tick
     * @return the Tick represented by the parameter JSON
     */
    public static Tick JSONToTick(String JSON) {
        return PRETTY_GSON.fromJson(JSON, Tick.class);
    }

    /**
     * Converts a Tick object to a JSON String
     * @param tick the Tick to be converted
     * @return a JSON String representing the parameter tick
     */
    public static String tickToJSON(Tick tick) {
        return PRETTY_GSON.toJson(tick);
    }

    /**
     * Returns a Response object from a JSON String
     * @param JSON the String representation of a Response
     * @return the Response represented by the parameter JSON
     */
    public static Response JSONToResponse(String JSON) {
        return PRETTY_GSON.fromJson(JSON, Response.class);
    }

    /**
     * Converts a Response object to a JSON String
     * @param response the Response to be converted
     * @return a JSON String representing the parameter response
     */
    public static String responseToJSON(Response response) {
        return PRETTY_GSON.toJson(response);
    }

    /**
     * Returns a Game object from a JSON String
     * @param JSON the String representation of a Game
     * @return the Game represented by the parameter JSON
     */
    public static Game JSONToGame(String JSON) {
        return PRETTY_GSON.fromJson(JSON, Game.class);
    }

    /**
     * Converts a Game object to a JSON String
     * @param game the Game to be converted
     * @return a JSON String representing the parameter game
     */
    public static String gameToJSON(Game game) {
        return PRETTY_GSON.toJson(game);
    }

    /**
     * Returns a TerminalTest object from a JSON String
     * @param JSON the String representation of a TerminalTest
     * @return the TerminalTest represented by the parameter JSON
     */
    public static TerminalTest JSONToTerminalTest(String JSON) {
        return REGULAR_GSON.fromJson(JSON, TerminalTest.class);
    }

    /**
     * Converts a TerminalTest object to a JSON String
     * @param terminalTest the Game to be converted
     * @return a JSON String representing the parameter TerminalTest
     */
    public static String terminalTestToJSON(TerminalTest terminalTest) {
        return REGULAR_GSON.toJson(terminalTest);
    }

}
