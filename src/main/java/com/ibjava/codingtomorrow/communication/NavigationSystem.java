package com.ibjava.codingtomorrow.communication;

import com.ibjava.codingtomorrow.controlclasses.Response;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.logic.navigation.Navigator;

/**
 * Implementing navigation system, as singleton, as only one instance of it will be used.
 * Wraps server communication and message sending logic into one class.
 */
public class NavigationSystem {

    private static NavigationSystem navigationSystem = new NavigationSystem();
    public static NavigationSystem getInstance() {
        return navigationSystem;
    }

    // Unique team token
    private static final String TOKEN = "{\"token\": \"OSQJgQEizDjzH43YWhm7P0TREmjHQRzUl9pqo6Xa6I0WE0pBEZ3YPydmJKtl9x2f0bm4qbWU\"}";
    private static final String FINALE = "{\"token\": \"OSQJgQEizDjzH43YWhm7P0TREmjHQRzUl9pqo6Xa6I0WE0pBEZ3YPydmJKtl9x2f0bm4qbWU\", \"finale\": \"lol\"}";
    private Client client;

    public static boolean WITH_KEY = true;

    private NavigationSystem() {
        client = new Client();
    }

    /**
     * Starts communication with the server by connecting to it and sending the team token.
     * The first tick of the game gets returned.
     * @return The first Tick of the game
     */
    public Tick startCommunication() {
        reset();
        client.startConnection();
        // the first tick as a json String which gets sent by the server after validating the team token
        String msg = WITH_KEY ? FINALE : TOKEN;
        System.out.println("msg = " + msg);
        String respJson = client.sendMessage(msg);
        System.out.println("respJson = " + respJson);
        // converting the json to actual Tick object
        return JSONProcessor.JSONToTick(respJson);
    }

    /**
     * Stops the communication with the server and creates a new client.
     */
    public void restartNavigationSystem() {
        client.stopConnection();
        client = new Client();
        Navigator.init();
    }

    /**
     * Sends a response to the server to the last tick.
     * The next tick gets returned.
     * @param response The response to send to the server
     * @return The next Tick of the game
     */
    public Tick respond(Response response) {
        // converting the parameter Response object to json String which the server can interpret
        String clientResponse = JSONProcessor.responseToJSON(response);
        // the response of the server in a json format
        String serverResponse = client.sendMessage(clientResponse);
        // converting the json to actual Tick object
        return JSONProcessor.JSONToTick(serverResponse);
    }

    /**
     * Reset to default values before new run
     */
    private static void reset() {
        Navigator.init();
    }

    /**
     * Given a Tick as a parameter, this method analyzes it and determines the best possible
     * response to it. This response gets returned.
     * @param tickToRespondTo The tick which is analyzed
     * @return a Response object to the parameter Tick
     */
    public static Response navigate(Tick tickToRespondTo) {

//        String command = NAVIGATOR.navigateOnRoute(tickToRespondTo, ROUTE.getPositions());
        String command = null;
        try {
            command = Navigator.getCommand(tickToRespondTo);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.println(tickToRespondTo.getRequest_id().getTick() + " - " + ROUTE.getPositions().get(0) + " -> " + ROUTE.getPositions().get(ROUTE.getPositions().size() - 1));
        System.out.println(tickToRespondTo.getRequest_id().getTick() + " - " + command);
        return new Response(tickToRespondTo, command);
    }

    public Client getClient() {
        return client;
    }
}
