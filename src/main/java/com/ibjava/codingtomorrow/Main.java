package com.ibjava.codingtomorrow;

import com.ibjava.codingtomorrow.communication.NavigationSystem;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.controlclasses.Response;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.history.HistoryManager;
import com.ibjava.codingtomorrow.ui.UIMain;


public class Main {

    public static void main(String[] args) {
        NavigationSystem.WITH_KEY = true;
        start(true);
    }

    private static void start(boolean withUI) {
        if (withUI) {
            UIMain.main(new String[]{});
        } else {
            runFromTerminal(true);
        }
    }

    public static Game runFromTerminal(boolean saveGameToHistory) {
        int count = 0;
        Game game = new Game();
        NavigationSystem navi = NavigationSystem.getInstance();
        try {
            Tick tick = navi.startCommunication();
            while (true) {
                Response resp = NavigationSystem.navigate(tick);
                game.add(tick, resp);
                tick = navi.respond(resp);
                if (tick.getRequest_id().getTick() == 2000) {
                    int x = 1/0;
                }
            }
        } catch (Exception e) {
            System.out.println(game);
            if (saveGameToHistory) HistoryManager.saveGameToHistory(game);
            e.printStackTrace();
            navi.getClient().stopConnection();
            return game;
        }
    }

}
