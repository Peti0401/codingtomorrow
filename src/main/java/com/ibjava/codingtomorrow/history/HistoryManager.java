package com.ibjava.codingtomorrow.history;

import com.ibjava.codingtomorrow.Utils;
import com.ibjava.codingtomorrow.communication.JSONProcessor;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.test.TerminalTest;

import java.io.File;
import java.util.Arrays;

public class HistoryManager {

    /**
     * Path to folder in which JSONs of single runs, Game objects are saved
     */
    public static final String RUN_HISTORY_PATH = "runHistory";

    /**
     * Path to folder in which JSONs of multiple runs, TerminalTest objects are saved
     */
    public static final String TEST_HISTORY_PATH = "testHistory";

    /**
     * Saves a game to history by writing the game's JSON to a file.
     * @param game the game to save
     */
    public static void saveGameToHistory(Game game) {
        // the file to which this game will be saved
        File file = new File(RUN_HISTORY_PATH + "/game_" + game.getID() + "(" + game.getNumTransported() + ")" + game.getStartPosition() +".json");
        String JSON = JSONProcessor.gameToJSON(game);
        Utils.writeToFile(file, JSON);
        System.out.println(game + " saved to history");
    }

    public static File[] getGameJSONFiles() {
        File runHistoryFolder = new File(RUN_HISTORY_PATH);
        // filter json files from the folder
        return runHistoryFolder.listFiles((dir, name) -> name.endsWith(".json"));
    }

    public static Game loadSingleGameFromJSON(File jsonFile) {
        String[] JSONLines = Utils.readFile(jsonFile);
        String fullJSON = String.join("\n", JSONLines);
        return JSONProcessor.JSONToGame(fullJSON);
    }

    /**
     * @return an array of Game objects from the dedicated history folder
     */
    public static Game[] loadGamesFromHistoy() {
        File[] JSONFiles = getGameJSONFiles();
        return Arrays
                .stream(JSONFiles)
                .map(HistoryManager::loadSingleGameFromJSON)
                .toArray(Game[]::new);
    }

    public static int getNumberOfSavedGames() {
        return getGameJSONFiles().length;
    }

    /**
     * Saves a TerminalTest to history by writing the TerminalTest's JSON to a file.
     * @param terminalTest the TerminalTest to save
     */
    public static void saveTerminalTestToHistory(TerminalTest terminalTest) {
        File file = new File(TEST_HISTORY_PATH + "/" + terminalTest + ".json");
        String JSON = JSONProcessor.terminalTestToJSON(terminalTest);
        Utils.writeToFile(file, JSON);
        System.out.println(terminalTest + " saved to history");
    }

    public static File[] getTerminalTestJSONFiles() {
        File runHistoryFolder = new File(TEST_HISTORY_PATH);
        // filter json files from the folder
        return runHistoryFolder.listFiles((dir, name) -> name.endsWith(".json"));
    }

    public static TerminalTest loadSingleTerminalTestFromJSON(File jsonFile) {
        String[] JSONLines = Utils.readFile(jsonFile);
        String fullJSON = String.join("\n", JSONLines);
        return JSONProcessor.JSONToTerminalTest(fullJSON);
    }

    /**
     * @return an array of TerminalTest objects from the dedicated history folder
     */
    public static TerminalTest[] loadTerminalTestsFromHistory() {
        File[] JSONFiles = getTerminalTestJSONFiles();
        return Arrays
                .stream(JSONFiles)
                .map(HistoryManager::loadSingleTerminalTestFromJSON)
                .toArray(TerminalTest[]::new);
    }

    public static int getNumberOfSavedTests() {
        return getTerminalTestJSONFiles().length;
    }


}
