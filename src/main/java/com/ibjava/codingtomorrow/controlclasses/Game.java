package com.ibjava.codingtomorrow.controlclasses;

import com.ibjava.codingtomorrow.entities.Car;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A Game can be clearly represented as an ordered list of Tick - Response pairs
 */
public class Game extends ArrayList<Pair<Tick, Response>> {

    public Game() {}

    public boolean add(Tick tick, Response response) {
        return super.add(new Pair<>(tick, response));
    }

    /**
     * @return the ticks starting from 0
     */
    public List<Tick> getTicks() {
        return  stream().map(Pair::getKey).collect(Collectors.toList());
    }

    /**
     * @return the commands in the order they were called
     */
    public List<String> getCommands() {
        return  stream().map(pair -> pair.getValue().command).collect(Collectors.toList());
    }

    public List<String> getGameDescription() {
        return  stream()
                .map(pair -> pair.getKey().request_id.tick + " - " + pair.getValue().command + " " + Arrays.toString(pair.getKey().messages))
                .collect(Collectors.toList());
    }

    /**
     * @return the position on which the car starts
     */
    public Position getStartPosition() {
        if (isEmpty()) return null;
        Tick firstTick = get(0).getKey();
        return firstTick.getCars()[0].getPos();
    }

    /**
     * @return the last message of the game if there is one
     */
    private String getLastMessage() {
        Tick lastTick = getLastTick();
        if (lastTick == null) return "No message";
        String[] messages = lastTick.getMessages();
        return messages == null || messages.length == 0 ? "No message" : Arrays.toString(messages);
    }

    /**
     * @return the last tick of the game or null if not existent
     */
    private Tick getLastTick() {
        List<Tick> ticks = getTicks();
        return ticks.isEmpty() ? null : ticks.get(ticks.size()-1);
    }

    /**
     * @return the game id or -1 if the tick list is empty
     */
    public int getID() {
        List<Tick> ticks = getTicks();
        return !ticks.isEmpty() ? ticks.get(0).request_id.game_id : -1;
    }

    public int getNumTransported() {
        Tick lastTick = getLastTick();
        Car ourCar;
        if (lastTick == null) {
            return -1;
        } else if ((ourCar = lastTick.getOurCar()) != null) {
            return ourCar.getTransported();
        } else {
            List<Tick> ticks = getTicks();
            Tick lastLivingTick = ticks.get(ticks.size()-2);
            return lastLivingTick.getOurCar().getTransported();
        }
    }

    @Override
    public String toString() {
        String lastMessage = getLastMessage();
        int numTicks = getTicks().size() - 1;
        return "Game #" + getID() + "(" + getNumTransported() +") - " + lastMessage + " at tick " + numTicks;
    }
}
