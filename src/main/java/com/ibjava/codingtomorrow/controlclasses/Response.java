package com.ibjava.codingtomorrow.controlclasses;

/**
 * Object for whole message which should be sent to the server
 */
public class Response {

    MetaData response_id;
    String command;

    public static final String NO_OP = "NO_OP";
    public static final String ACCELERATION = "ACCELERATION";
    public static final String DECELERATION = "DECELERATION";
    public static final String CAR_INDEX_LEFT = "CAR_INDEX_LEFT";
    public static final String CAR_INDEX_RIGHT = "CAR_INDEX_RIGHT";
    public static final String CLEAR = "CLEAR";
    public static final String FULL_THROTTLE = "FULL_THROTTLE";
    public static final String EMERGENCY_BRAKE = "EMERGENCY_BRAKE";
    public static final String GO_LEFT = "GO_LEFT";
    public static final String GO_RIGHT = "GO_RIGHT";

    public Response() {}

    /**
     * Creates a response with the starting metadata and properly incremented tick count.
     * @param tick The tick to which we are responding
     * @param command The command to be executed
     */
    public Response(Tick tick, String command) {
        this(tick.getRequest_id(), command);
    }

    public Response(MetaData metaData, String command) {
        this.response_id = metaData;
        this.command = command;
    }

    public Response(int game_id, int tick, int car_id, String command) {
        this(new MetaData(game_id, tick, car_id), command);
    }

    public MetaData getResponse_id() {
        return response_id;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return "Response{" + "\n" +
                "\tresponse_id=" + response_id + "\n" +
                "\tcommand='" + command + '\'' + "\n" +
                '}';
    }
}
