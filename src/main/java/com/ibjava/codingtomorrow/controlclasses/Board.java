package com.ibjava.codingtomorrow.controlclasses;

import com.ibjava.codingtomorrow.Utils;

import java.io.File;
import java.util.Arrays;

/**
 * Implementing the Board class as a Singleton, because only one instance will be needed.
 * The character matrix representing the BOARD_UI is static, it won't be changed on runtime.
 *
 * S = Street - normal field
 * Z = Zebra - normal field, cross for pedestrians
 * P = Pedestrian area - special field: speed is reduced to 0 and life is reduced by max(0, 2*speed-2)-1 / tick
 * G = Green area - special field: speed is reduced to 0 and life is reduced by max(0, 2*speed-2)-1 / tick
 * B = Building - forbidden field
 * T = Tree - forbidden field
 *
 */
public class Board {

    private static String TXT_PATH = "src/main/resources/com/ibjava/codingtomorrow/control/board.txt";
    private static Board board = new Board();

    private char[][] charMatrix;

    private static Character[] GO_ZONE = {'S', 'Z', 'X'};
    private static Character[] GO_ZONE_PEDESTRIAN = {'S', 'Z', 'P', 'C'}; //pedestrians walk on these
    private static Character[] ROAD = {'S', 'Z', 'X'};
    private static Character[] OBSTACLES = {'T', 'B', 'R', 'C'};
    private static Character[] ZEBRA = {'Z'};
    private static Character[] CAR_RAIL_CROSS = {'X'};

    public static Board getInstance() {
        return board;
    }

    /**
     * Reads the char matrix from a text file and initializes its char[][] field accordingly
     */
    private Board() {
        String[] boardRows = Utils.readFile(new File(TXT_PATH));
        charMatrix =
                Arrays
                .stream(boardRows)
                .map(String::toCharArray)
                .toArray(char[][]::new);
    }

    public int numRows() {
        return charMatrix.length;
    }

    public int numCols() {
        return charMatrix[0].length;
    }

    public char[][] getCharMatrix() {
        return charMatrix;
    }

    public static Character getCharacterAtPosition(Position position) {
        // x and y are switched because row and col is the other way around
        return Board.getInstance().getCharMatrix()[position.getY()][position.getX()];
    }

    static boolean isGoZone(Position position) {
        return Arrays.asList(GO_ZONE).contains(getCharacterAtPosition(position));
    }

    static boolean isRoad(Position position) {
        return Arrays.asList(ROAD).contains(getCharacterAtPosition(position));
    }

    static boolean isObstacle(Position position) {
        return Arrays.asList(OBSTACLES).contains(getCharacterAtPosition(position));
    }

    static boolean isZebra(Position position) {
        return Arrays.asList(ZEBRA).contains(getCharacterAtPosition(position));
    }

    static boolean isCarRailCross(Position position) {
        return Arrays.asList(CAR_RAIL_CROSS).contains(getCharacterAtPosition(position));
    }

    static boolean isGoZoneForPedestrian(Position position) {
        return Arrays.asList(GO_ZONE_PEDESTRIAN).contains(getCharacterAtPosition(position));
    }

}
