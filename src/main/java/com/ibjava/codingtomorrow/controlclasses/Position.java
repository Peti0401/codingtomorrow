package com.ibjava.codingtomorrow.controlclasses;

import com.ibjava.codingtomorrow.logic.analyser.MapAnalyser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents a simple [x,y] coordinate.
 * Bytes are used instead of ints, since values range only between 0 and 60
 */
public class Position {

    private byte x;
    private byte y;

    public Position(byte x, byte y) {
        this.x = x;
        this.y = y;
    }

    public Position(int x, int y) {
        this(((byte) x), ((byte) y));
    }

    public Position(Position position) {
        this.x = position.getX();
        this.y = position.getY();
    }

    public byte getX() {
        return x;
    }

    public byte getY() {
        return y;
    }

    /**
     * Translates a position by a specific amount on the x- and y-axis
     * @param translateX move on x-axis. (positive ? right : left)
     * @param translateY move on y-axis. (positive ? down : up)
     * @return the translated position
     */
    private Position translate(byte translateX, byte translateY) {
        Position newPos = new Position(this);

        byte lowerBound_X = 0;
        byte lowerBound_Y = 0;
        byte upperBound_X = (byte)(Board.getInstance().getCharMatrix()[0].length-1); // column end
        byte upperBound_Y = (byte)(Board.getInstance().getCharMatrix().length-1); // row end

        byte newX = (byte)(getX() + translateX);
        byte newY = (byte)(getY() + translateY);

        boolean isWithinXBoundaries = lowerBound_X <= newX && upperBound_X >= newX;
        boolean isWithinYBoundaries = lowerBound_Y <= newY && upperBound_Y >= newY;

        // correct X coordinate with overlap on each side
        if (!isWithinXBoundaries) {
            newX = (byte) Math.abs(upperBound_X + 1 - Math.abs(newX));
        }
        newPos.x = newX;

        // correct Y coordinate with overlap top-bottom
        if (!isWithinYBoundaries) {
            newY = (byte) Math.abs(upperBound_Y + 1 - Math.abs(newY));
        }
        newPos.y = newY;

        return newPos;
    }

    /**
     * Translates a Position to a given direction by a given amount of steps
     * @param direction translate direction
     * @param steps magnitude of translation
     * @return the translated position
     */
    public Position translateToDirection(Direction direction, byte steps) {

        switch (direction) {
            case DOWN: // x-axis: 0, y-axis: steps
                return translate((byte) 0, steps);
            case UP: // x-axis: 0, y-axis: -steps
                return translate((byte) 0, (byte) -steps);
            case RIGHT: // x-axis: steps, y-axis: 0
                return translate(steps, (byte) 0);
            case LEFT: // x-axis: -steps, y-axis: 0
                return translate((byte) -steps, (byte) 0);
            default:
                return null; // happens only if param direction == null
        }
    }

    public Position translateToDirectionWithoutOverlap(Direction direction, byte steps) {
        switch (direction) {
            case DOWN: // x-axis: 0, y-axis: steps
                return new Position(x, y + steps);
            case UP: // x-axis: 0, y-axis: -steps
                return new Position(x, y - steps);
            case RIGHT: // x-axis: steps, y-axis: 0
                return new Position(x + steps, y);
            case LEFT: // x-axis: -steps, y-axis: 0
                return new Position(x - steps, y);
            default:
                return null; // happens only if param direction == null
        }
    }

    /**
     * @return an array of the 4 adjacent positions to this position
     */
    public Position[] getAdjacentPositions() {
        Position[] positions = new Position[4];

        for (int i = 0; i < 4; i++) {
            positions[i] = this.translateToDirection(Direction.getDirection((byte) i), (byte) 1);
        }

        return positions;
    }

    /**
     * @param position
     * @return the (param) position's relative direction to this position
     */
    public Direction getDirectionTo(Position position) {
        // up
        if (this.x == position.getX() && this.y == position.translateToDirection(Direction.DOWN, (byte) 1).getY()) {
            return  Direction.UP;
        }

        // right
        if (this.x == position.translateToDirection(Direction.LEFT, (byte) 1).getX() && this.y == position.getY()) {
            return Direction.RIGHT;
        }

        // down
        if (this.x == position.getX() && this.y == position.translateToDirection(Direction.UP, (byte) 1).getY()) {
            return Direction.DOWN;
        }

        // left
        if (this.x == position.translateToDirection(Direction.RIGHT, (byte) 1).getX() && this.y == position.getY()) {
            return  Direction.LEFT;
        }

        return null;
    }

    public List<Direction> getDirectionToNotAdjacentPosition(Position position) {
        List<Direction> directions = new ArrayList<>();

        try {
            if (x > position.getX()) {
                directions.add(Direction.LEFT);
            } else if (x < position.getX()) {
                directions.add(Direction.RIGHT);
            }
            if (y > position.getY()) {
                directions.add(Direction.UP);
            } else if (y < position.getY()) {
                directions.add(Direction.DOWN);
            }

            if (this.equals(position)) {
                directions.addAll(Arrays.asList(Direction.values()));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return directions;
    }

    /**
     * Positions must be next to each other
     * @param position1
     * @param position2
     * @return
     */
    public static boolean isOverlapping(Position position1, Position position2) {
        Direction direction = position1.getDirectionTo(position2);
        if (direction == null) return false;
        if (direction.isHorizontal()) {
            return Math.abs(position1.getX() - position2.getX()) != 1;
        } else { // vertical
            return Math.abs(position1.getY() - position2.getY()) != 1;
        }
    }

    public boolean isValid() {
        return (x >= 0 && y >= 0) && (x < Board.getInstance().numCols() && y < Board.getInstance().numRows());
    }

    public boolean isGoZone() { return Board.isGoZone(this); }
    public boolean isRoad() { return Board.isRoad(this); }
    public boolean isObstacle() {return  Board.isObstacle(this); }
    public boolean isZebra() { return Board.isZebra(this); }
    public boolean isCarRailCross() { return Board.isCarRailCross(this); }
    public boolean isGoZoneForPedestrian() { return Board.isGoZoneForPedestrian(this); }

    public boolean isInRoundAbout() {
        return MapAnalyser.isInRoundAbout(this);
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Position) {
            Position position = (Position) obj;
            return this.x == position.getX() && this.y == position.getY();
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + x + ";" + y + "]";
    }
}
