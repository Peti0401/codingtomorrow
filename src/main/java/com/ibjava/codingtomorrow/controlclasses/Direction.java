package com.ibjava.codingtomorrow.controlclasses;

import java.util.Arrays;

public enum Direction {
    UP('^', (byte) 0),
    RIGHT('>', (byte) 1),
    DOWN('v', (byte) 2),
    LEFT('<', (byte) 3);

    public final Character CH;
    public final byte I;

    Direction(Character CH, byte I) {
        this.CH = CH;
        this.I = I;
    }

    public static boolean isHorizontal(Direction direction) {
        return direction == LEFT || direction == RIGHT;
    }

    public boolean isHorizontal() {
        return isHorizontal(this);
    }

    public static boolean isVertical(Direction direction) {
        return direction == UP || direction == DOWN;
    }

    public boolean isVertical() {
        return isVertical(this);
    }

    /**
     * Returns the direction between two adjacent coordinates
     * @param from
     * @param to
     * @return
     */
    public static Direction getDirection(Position from, Position to) {
        return from.getDirectionTo(to);
    }

    /**
     * Returns a sequence of directions from an array of positions
     * @param positions
     * @return
     */
    public static Direction[] positionsToDirections(Position[] positions) {
        Direction[] directions = new Direction[positions.length-1];
        for (int i = 0; i < positions.length-1; i++) {
            Position from = positions[i];
            Position to = positions[i+1];
            directions[i] = getDirection(from, to);
        }
        return directions;
    }

    /**
     *
     * @param direction the Direction of which we want to get the inverse
     * @return the inverse Direction
     */
    public static Direction getInverse(Direction direction) {
        switch (direction) {
            case UP:
                return DOWN;
            case RIGHT:
                return LEFT;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            default:
                return null;
        }
    }

    public Direction getInverse() {
        return getInverse(this);
    }

    /**
     * @param i the index of the direction we want to get
     * @return the corresponding Direction or null if
     */
    public static Direction getDirection(byte i) {
        return Arrays
                .stream(values())
                .filter(direction -> direction.I == i % 4)
                .toArray(Direction[]::new)[0];
    }

    /**
     *
     * @param heading the heading to which we want to get the relative Direction
     * @param relativeDirection the relative Direction to the heading
     * @return the actual Direction according to the board
     */
    public static Direction getRelativeDirection(Direction heading, Direction relativeDirection) {
        return getDirection((byte) (heading == null ? relativeDirection.I : relativeDirection.I + heading.I));
    }

    /**
     * @param oldDirection direction to turn from
     * @param newDirection direction to turn to
     * @return An array of commands performing the desired turn
     */
    public static String[] changeDirectionTo(Direction oldDirection, Direction newDirection) {
        // UP = 0
        // RIGHT = 1
        // DOWN = 2
        // LEFT = 3
        String[] commands = null;
        switch (oldDirection.I - newDirection.I) {
            case -3: // UP -> LEFT
            case 1: // RIGHT -> UP || DOWN -> RIGHT || LEFT -> DOWN
                commands = new String[]{Response.CAR_INDEX_LEFT};
                break;
            case 3: // LEFT -> UP
            case -1: // UP -> RIGHT || RIGHT -> DOWN || DOWN -> LEFT
                commands = new String[]{Response.CAR_INDEX_RIGHT};
                break;
            case -2: // UP -> DOWN || RIGHT -> LEFT
            case 2: // DOWN -> UP || LEFT -> RIGHT
                commands = new String[]{Response.CAR_INDEX_LEFT, Response.CAR_INDEX_LEFT};
                break;
            case 0: // no change
                commands = new String[]{};
                break;
        }
        return commands;
    }

}
