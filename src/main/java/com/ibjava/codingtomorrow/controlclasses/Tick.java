package com.ibjava.codingtomorrow.controlclasses;

import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.entities.Pedestrian;

import java.util.Arrays;

/**
 * Represents a tick with all the available information.
 * Used purely for JSON generation, therefore only a default constructor is needed.
 */
public class Tick {

    MetaData request_id;
    Car[] cars;
    Pedestrian[] pedestrians;
    Passenger[] passengers;
    String[] messages;

    public Tick() {}

    public MetaData getRequest_id() {
        return request_id;
    }

    public Car[] getCars() {
        return cars;
    }

    public Car getOurCar() {
        if (cars.length == 0) {
            return null;
        }
        int ourCarsId = request_id.car_id;
        return Arrays
                .stream(cars)
                .filter(car -> car.getId() == ourCarsId)
                .toArray(Car[]::new)[0];
    }

    public Pedestrian[] getPedestrians() {
        return pedestrians;
    }

    public Passenger[] getPassengers() {
        return passengers;
    }

    public String[] getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "Tick{" + "\n" +
                "\trequest_id=" + request_id + "\n" +
                "\tcars=" + Arrays.toString(cars) + "\n" +
                "\tpedestrians=" + Arrays.toString(pedestrians) + "\n" +
                "\tpassengers=" + Arrays.toString(passengers) + "\n" +
                "\tmessages=" + Arrays.toString(messages) + "\n" +
                '}';
    }
}
