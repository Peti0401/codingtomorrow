package com.ibjava.codingtomorrow.controlclasses;

/**
 * Object for tick info
 */
public class MetaData {

    int game_id; // current game's id
    int tick; // current tick
    int car_id; // controlled car's id

    public MetaData() { }

    public MetaData(int game_id, int tick, int car_id) {
        this.game_id = game_id;
        this.tick = tick;
        this.car_id = car_id;
    }

    public int getGame_id() {
        return game_id;
    }

    public int getTick() {
        return tick;
    }

    public int getCar_id() {
        return car_id;
    }

    @Override
    public String toString() {
        return "MetaData{" + "\n" +
                "\tgame_id=" + game_id + "\n" +
                "\ttick=" + tick + "\n" +
                "\tcar_id=" + car_id + "\n" +
                '}';
    }
}
