package com.ibjava.codingtomorrow.logic.analyser;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Train;
import com.ibjava.codingtomorrow.logic.navigation.Section;
import com.ibjava.codingtomorrow.logic.navigation.Step;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapAnalyser {

    private static final int TRAIN_NORTH_Y = 5;
    private static final int TRAIN_SOUTH_Y = 54;
    private static final int TRAIN_WEST_X = 5;
    private static final int TRAIN_EAST_X = 54;

    public static final List<Train> TRAINS = Arrays.asList // DO NOT CHANGE THE ORDER OF THE TRAINS
            (
                    new Train // North Train
                            (
                                    2, new Position[]{new Position((byte)0, (byte)5), new Position((byte)1, (byte)5)},
                                    24, new Position[]{new Position((byte)58, (byte)5), new Position((byte)59, (byte)5)},
                                    Direction.RIGHT, Train.CompassPoint.NORTH
                            ),
                    new Train // South Train
                            (
                                    2, new Position[]{new Position((byte)59, (byte)54), new Position((byte)58, (byte)54)},
                                    24, new Position[]{new Position((byte)1, (byte)54), new Position((byte)0, (byte)54)},
                                    Direction.LEFT, Train.CompassPoint.SOUTH
                            ),
                    new Train // West Train
                            (
                                    27, new Position[]{new Position((byte)5, (byte)59), new Position((byte)5, (byte)58)},
                                    49, new Position[]{new Position((byte)5, (byte)1), new Position((byte)5, (byte)0)},
                                    Direction.UP, Train.CompassPoint.WEST
                            ),
                    new Train // East Train
                            (
                                    27, new Position[]{new Position((byte)54, (byte)0), new Position((byte)54, (byte)1)},
                                    49, new Position[]{new Position((byte)54, (byte)58), new Position((byte)54, (byte)59)},
                                    Direction.DOWN, Train.CompassPoint.EAST
                            )
            );

    public static boolean isEnteringZebra(Car car, int stepsTillZebra) {
        Direction direction = car.getDirection();
        Position ahead =
                car
                .getPos()
                .translateToDirection(direction, (byte) stepsTillZebra);
        return ahead.isZebra();
    }

    public static boolean isEnteringRoundabout(Car car) {
        Direction direction = car.getDirection();
        Position ahead = car.getPos()
                .translateToDirection(direction, (byte) 1);

        return !isInRoundAbout(car.getPos()) && isInRoundAbout(ahead);
    }

    public static boolean isEnteringRailCross(Car car) {
        Position positionAhead = car.getPos()
                                    .translateToDirection(car.getDirection(), (byte) 1);

        return car.getSpeed() <= 1 && positionAhead.isCarRailCross();
    }

    public static boolean isInRoundAbout(Position position) {
        Position leftUpOfRoundabout = new Position(position);

        Direction[] directions = new Direction[]{Direction.UP, Direction.LEFT, Direction.DOWN, Direction.RIGHT};
        for (Direction direction : directions) {
            for (int i = 0; i < 4; i++) {
                if (isRoundabout(leftUpOfRoundabout)) {
                    return true;
                }
                leftUpOfRoundabout = leftUpOfRoundabout.translateToDirection(direction, (byte) 1);
            }
            leftUpOfRoundabout = leftUpOfRoundabout.translateToDirection(Direction.getInverse(direction), (byte) 1);
        }

        return false;
    }

    public static boolean isCrossingDisjunction(Car car, Section section) {
        return car.getSpeed() <= 1 && !MapAnalyser.isInRoundAbout(car.getPos())
                && (section == null || section.getStartDirection() == car.getDirection());
    }

    // ===================== right side ============================

    public static boolean isOnTheRightSide(Step step) {
        // left and right side are roads

        Direction relativeLeft = Direction.getRelativeDirection(step.getHeading(), Direction.LEFT);
        Direction relativeRight = Direction.getRelativeDirection(step.getHeading(), Direction.RIGHT);

        int roadToLeft = Math.min(
                getRoadsToDirection(step.getPosition(), relativeLeft),
                getRoadsToDirection(step.getPreviousStep().getPosition(), relativeLeft)
        );
        int roadToRight = Math.min(
                getRoadsToDirection(step.getPosition(), relativeRight),
                getRoadsToDirection(step.getPreviousStep().getPosition(), relativeRight)
        );

        int widthOfTheRoad = roadToLeft + roadToRight + 1;

        // we are on the right side
        return widthOfTheRoad / 2 <= roadToLeft || roadToLeft > 0;
    }

    public static int getRoadsToDirection(Position position, Direction direction) {
        int roadToDirection = 0;

        Position translatedPosition = new Position(position);

        do {
            roadToDirection++;
            translatedPosition = translatedPosition.translateToDirection(direction, (byte) 1);
        } while (translatedPosition.isRoad() && !position.equals(translatedPosition));

        roadToDirection--;

        return roadToDirection;
    }

    // ===================== right side ============================

    // ===================== roundabout ============================

    private static final Map<Integer, List<Integer>> ROUNDABOUT_ROADS = new HashMap<Integer, List<Integer>>(){
        {
            put(0, Arrays.asList(0, 1, 2, 3));
            put(1, Arrays.asList(0, 3));
            put(2, Arrays.asList(0, 3));
            put(3, Arrays.asList(0, 1, 2, 3));
        }
    };

    private static final Map<Integer, List<Direction[]>> ROUNDABOUT_DIRECTIONS = new HashMap<Integer, List<Direction[]>>(){
        {
            put(0, Arrays.asList(
                    new Direction[]{Direction.DOWN, Direction.LEFT},
                    new Direction[]{Direction.LEFT, Direction.DOWN},
                    new Direction[]{Direction.LEFT, Direction.DOWN},
                    new Direction[]{Direction.LEFT, Direction.UP}
            ));
            put(1, Arrays.asList(
                    new Direction[]{Direction.DOWN, Direction.RIGHT},
                    new Direction[]{Direction.UP, Direction.LEFT}
            ));
            put(2, Arrays.asList(
                    new Direction[]{Direction.DOWN, Direction.RIGHT},
                    new Direction[]{Direction.UP, Direction.LEFT}
            ));
            put(3, Arrays.asList(
                    new Direction[]{Direction.RIGHT, Direction.DOWN},
                    new Direction[]{Direction.RIGHT, Direction.UP},
                    new Direction[]{Direction.RIGHT, Direction.UP},
                    new Direction[]{Direction.UP, Direction.RIGHT}
            ));
        }
    };

    private static final Map<Integer, List<Integer>> ROUNDABOUT_MIDDLE = new HashMap<Integer, List<Integer>>(){
        {
            put(1, Arrays.asList(1, 2));
            put(2, Arrays.asList(1, 2));
        }
    };

    private static boolean isRoundabout(Position leftUpEdge) {

        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                try {
                    Position position = new Position((byte) (leftUpEdge.getX() + x), (byte) (leftUpEdge.getY() + y));

                    if (ROUNDABOUT_ROADS.containsKey(y) && ROUNDABOUT_ROADS.get(y).contains(x)) {
                        // should be road
                        if (!position.isRoad()) {
                            return false;
                        }
                    } else if (ROUNDABOUT_MIDDLE.containsKey(y) && ROUNDABOUT_MIDDLE.get(y).contains(x)) {
                        // should not be road
                        if (position.isRoad()) {
                            return false;
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    return false;
                }
            }
        }

        return true;
    }

    public static Direction[] getPossibleDirectionInRoundabout(Position position) {
        Position leftUpOfRoundabout = new Position(position);

        Direction[] directions = new Direction[]{Direction.UP, Direction.LEFT, Direction.DOWN, Direction.RIGHT};
        for (Direction direction : directions) {
            for (int i = 0; i < 4; i++) {
                if (isRoundabout(leftUpOfRoundabout)) {
                    // TODO not working if the roundabout is at the overlap
                    int x = Math.abs(position.getX() - leftUpOfRoundabout.getX());
                    int y = Math.abs(position.getY() - leftUpOfRoundabout.getY());

                    int indexOfDirection = ROUNDABOUT_ROADS.get(y).indexOf(x);

                    Direction[] possibleDirection = ROUNDABOUT_DIRECTIONS.get(y).get(indexOfDirection);
                    return possibleDirection;
                }

                leftUpOfRoundabout = leftUpOfRoundabout.translateToDirection(direction, (byte) 1);
            }

            leftUpOfRoundabout = leftUpOfRoundabout.translateToDirection(Direction.getInverse(direction), (byte) 1);
        }

        return null;
    }

    // ===================== roundabout ============================

    /**
     * @param position a position that is part of a railway
     * @return the corresponding train
     */
    public static Train getCrossingTrain(Position position) {
        int x = position.getX();
        int y = position.getY();

        if (y == TRAIN_NORTH_Y) {
            return TRAINS.get(0);
        } else if (y == TRAIN_SOUTH_Y) {
            return TRAINS.get(1);
        } else if (x == TRAIN_WEST_X) {
            return TRAINS.get(2);
        } else if (x == TRAIN_EAST_X) {
            return TRAINS.get(3);
        }

        return null;
    }

}
