package com.ibjava.codingtomorrow.logic.simulator;

import com.ibjava.codingtomorrow.controlclasses.*;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.entities.Pedestrian;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.ibjava.codingtomorrow.controlclasses.Response.*;

/**
 * class for static methods that predict the next stage of entities
 */
public class Simulator {

    /**
     * simulates a command on the car
     * @param command the command to simulate
     * @param tick the tick from which we retrieve the car and passengers etc.
     * @return a new car object which represents the original car after the command
     */
    public static Car simulateCommand(String command, Tick tick) {
        final Car nextCar = new Car(tick.getOurCar());
        if (command == null) return nextCar;

        int stepsAhead;
        Direction direction;
        Position position;

        switch (command) {
            case NO_OP:
                direction = nextCar.getDirection();
                stepsAhead = nextCar.getSpeed();

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);
                break;
            case DECELERATION:
                direction = nextCar.getDirection();
                stepsAhead = Math.max(nextCar.getSpeed() - 1, 0);

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                nextCar.decelerate();
                break;
            case ACCELERATION:
                direction = nextCar.getDirection();
                stepsAhead = Math.min(nextCar.getSpeed() + 1, nextCar.getMaxSpeed());

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                nextCar.accelerate();
                break;
            case CAR_INDEX_LEFT:
            case CAR_INDEX_RIGHT:
                direction = nextCar.getDirection();
                stepsAhead = nextCar.getSpeed() > 1 ? nextCar.getSpeed() - 1 : nextCar.getSpeed();

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                Direction newDirection = Direction.getRelativeDirection(
                        direction,
                        command.equals(Response.CAR_INDEX_RIGHT) ? Direction.RIGHT : Direction.LEFT
                );

                nextCar.setDirection(newDirection);

                if (nextCar.getSpeed() > 1) {
                    nextCar.decelerate();
                }
                break;
            case EMERGENCY_BRAKE:
                direction = nextCar.getDirection();
                stepsAhead = Math.max(nextCar.getSpeed() - 1, 0);

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                nextCar.decelerate();
                break;
        }
        //TODO other commands that take health

        if (nextCar.getSpeed() == 0 && !nextCar.hasPassenger()) {
            Position[] adjacentPositions = nextCar.getPos().getAdjacentPositions();

            for (Position adjacentPosition : adjacentPositions) {
                Passenger[] passengers = tick.getPassengers();

                for (Passenger passenger : passengers) {
                    if (adjacentPosition.equals(passenger.getPos())) {
                        nextCar.setPassenger_Id(passenger.getId());
                        break;
                    }
                }
            }
        } else if (nextCar.getSpeed() == 0 && nextCar.hasPassenger()) {

            Position[] adjacentPositions = nextCar.getPos().getAdjacentPositions();

            for (Position adjacentPosition : adjacentPositions) {
                if (adjacentPosition.equals(nextCar.getPassenger(tick.getPassengers()).getDest_pos())) {
                    nextCar.setPassenger_Id(0);
                    break;
                }
            }
        }

        return nextCar;
    }

    public static Car simulateCommand(String command, Car car) {
        final Car nextCar = new Car(car);
        if (command == null) return nextCar;

        int stepsAhead;
        Direction direction;
        Position position;

        switch (command) {
            case NO_OP:
                direction = nextCar.getDirection();
                stepsAhead = nextCar.getSpeed();

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);
                break;
            case DECELERATION:
                direction = nextCar.getDirection();
                stepsAhead = Math.max(nextCar.getSpeed() - 1, 0);

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                nextCar.decelerate();
                break;
            case ACCELERATION:
                direction = nextCar.getDirection();
                stepsAhead = Math.min(nextCar.getSpeed() + 1, nextCar.getMaxSpeed());

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                nextCar.accelerate();
                break;
            case CAR_INDEX_LEFT:
            case CAR_INDEX_RIGHT:
                direction = nextCar.getDirection();
                stepsAhead = nextCar.getSpeed() > 1 ? nextCar.getSpeed() - 1 : nextCar.getSpeed();

                position = nextCar.getPos().translateToDirection(direction, (byte) stepsAhead);
                nextCar.setPosition(position);

                Direction newDirection = Direction.getRelativeDirection(
                        direction,
                        command.equals(Response.CAR_INDEX_RIGHT) ? Direction.RIGHT : Direction.LEFT
                );

                nextCar.setDirection(newDirection);

                if (nextCar.getSpeed() > 1) {
                    nextCar.decelerate();
                }
                break;
        }

        return nextCar;
    }

    /**
     * simulates the next move of the pedestrians (worst case scenario) -> if a pedestrian
     * is near a zebra but not headed in that direction, move them there
     * anyway - else - move them along the heading direction
     *
     * @param pedestrians of which the next move is going to be simulated
     * @return the list of simulated pedestrians
     */
    public static List<Pedestrian> simulatePedestrians(Pedestrian[] pedestrians) {
        return Arrays
                .stream(pedestrians)
                .map(Simulator::simulatePedestrian)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    /**
     * simulates the next move of the pedestrian (worst case scenario) -> if a pedestrian
     * is near a zebra but not headed in that direction, move them there
     * anyway - else - move them along the heading direction
     *
     * @param pedestrian of which the next move is going to be simulated
     * @return the list of possible new positions of the pedestrian (size more than one
     *          only if the pedestrian can step on two or more zebras in the next tick)
     */
    private static List<Pedestrian> simulatePedestrian(Pedestrian pedestrian) {
        List<Pedestrian> simulatedPedestrians = new ArrayList<>();
        Position[] adjacentPositions = pedestrian.getPos().getAdjacentPositions();

        for (Position adjacentPosition : adjacentPositions) {
            if (adjacentPosition.isGoZoneForPedestrian()) {
                Pedestrian simulatedPedestrian = new Pedestrian(pedestrian);
                Direction direction = Direction.getDirection(pedestrian.getPos(), adjacentPosition);

                simulatedPedestrian.setPosition(adjacentPosition);
                simulatedPedestrian.setDirection(direction);

                simulatedPedestrians.add(simulatedPedestrian);
            }
        }

        return simulatedPedestrians;
    }

    /**
     *
     * @param command
     * @param originalTick
     * @return
     */
    public static Tick simulateTick(String command, Tick originalTick) {
        Tick simulatedTick = new Tick();

        //TODO simulate whole tick (???is it necessary tho???)

        return simulatedTick;
    }

}
