package com.ibjava.codingtomorrow.logic.route;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.logic.navigation.PathFinder;
import com.ibjava.codingtomorrow.logic.navigation.Section;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Route {

    private Position start;
    private Position end;
    private List<Position> positions;
    private List<Section> sections;
    private int passenger_id;

    public Route(Position start, Position end, Passenger[] passengers, int passenger_id, List<Direction> startDirections) {
        this.start = new Position(start);
        this.end = new Position(end);
        this.passenger_id = passenger_id;

        // new start directions
        if (start.isRoad()) {
            positions = new ArrayList<>(Arrays.asList(PathFinder.getPath_escape(start, end, passengers, startDirections.toArray(new Direction[0]))));
        } else {
            positions = new ArrayList<>(Arrays.asList(PathFinder.getPath(start, end, passengers, startDirections.toArray(new Direction[0]))));
        }

        sections = Section.generateSections(positions);
    }

    public Position getStart() {
        return new Position(start);
    }

    public Position getEnd() {
        return new Position(end);
    }

    public boolean isPassengerAtEnd(Passenger[] passengers) {
        List<Position> endAdjacentPositions = Arrays.asList(getEnd().getAdjacentPositions());
        return Arrays
                .stream(passengers)
                .anyMatch(passenger -> endAdjacentPositions.contains(passenger.getPos()));
    }


    public int getPassenger_id() {
        return passenger_id;
    }

    public int getLength() {
        return positions.size();
    }

    public List<Section> getSections() {
        return new ArrayList<>(sections);
    }

    public boolean isFinished(Car car) {
        return isOnRoute(car) && end.equals(car.getPos()) && car.getSpeed() == 0;
    }

    public boolean isOnRoute(Car car) {
        return positions.stream().anyMatch(position -> position.equals(car.getPos()));
    }

    public Section getCurrentSection(Car car) {
        Section section = null;

        for (Section value : sections) {

            if (value.isInSection(car.getPos())) {
                section = value;
                break;
            }
        }

        return section;
    }

    public boolean isCrossingRail() {
        return positions.stream().anyMatch(Position::isCarRailCross);
    }

}
