package com.ibjava.codingtomorrow.logic.route;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.logic.navigation.PathFinder;
import com.ibjava.codingtomorrow.logic.route.Route;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class RouteFactory {

    public static Route routeToDropPassenger(Car car, Passenger[] passengers, List<Direction> startDirections) {
        Route route;

        Passenger passenger = car.getPassenger(passengers);

        System.err.println("calculating route to drop passenger");

        Position start = car.getPos();
        Position end = PathFinder.getAdjacentRoad(passenger.getDest_pos());

        route = new Route(start, end, passengers, passenger.getId(), startDirections);

        System.out.println("drop passenger passenger: " + passenger.getId());
        System.err.println("from: " + start + " -> to: " + end);

        return route;
    }

    public static Route routeToPickUpPassenger(@NotNull Car car, Passenger[] passengers, @NotNull Passenger passengerToPickUp, List<Direction> startDirections) {
        Route route;

        System.err.println("calculating route to pick up passenger");

        Position start = car.getPos();
        Position end = PathFinder.getAdjacentRoad(passengerToPickUp.getPos());

        route = new Route(start, end, passengers, passengerToPickUp.getId(), startDirections);

        System.out.println("pick up passenger: " + passengerToPickUp.getId());
        System.err.println("from: " + start + " -> to: " + end);

        return route;
    }

    public static Route routeToPickUpSafestPassenger(Car car, Passenger[] passengers, int bannedId, List<Direction> startDirections) {
        Route route = null;

        Passenger[] saferOnes = Arrays
                .stream(passengers)
                .filter(passenger -> PathFinder.getAdjacentRoad(passenger.getPos()).isGoZone())
                .filter(passenger -> passenger.getId() != bannedId)
                .toArray(Passenger[]::new);

        Passenger[] others = Arrays
                .stream(passengers)
                .filter(passenger -> passenger.getId() != bannedId)
                .toArray(Passenger[]::new);

        Passenger[] passengersToChooseFrom = saferOnes.length > 0 ? saferOnes : others;

        for (Passenger passenger : passengersToChooseFrom) {
            Route routeToPickUp = routeToPickUpPassenger(car, passengers, passenger, startDirections);

            if ( (route == null || route.getLength() > routeToPickUp.getLength()) && routeToPickUp.getLength() > 1 ) {
                route = routeToPickUp;
            }
        }

        if (route == null) {
            System.err.println("we have a problem");
        }

        return route;
    }

    public static Route routeToPickUpClosestPassenger(Car car, Passenger[] passengers, int bannedId, List<Direction> startDirections) {
        Route route = null;

        Passenger[] passengersToChooseFrom = Arrays
                .stream(passengers)
                .filter(passenger -> passenger.getId() != bannedId)
                .toArray(Passenger[]::new);

        for (Passenger passenger : passengersToChooseFrom) {
            Route routeToPickUp = routeToPickUpPassenger(car, passengers, passenger, startDirections);

            if ( (route == null || route.getLength() > routeToPickUp.getLength()) && routeToPickUp.getLength() > 1 ) {
                route = routeToPickUp;
            }
        }

        if (route == null) {
            System.err.println("we have a problem");
        }

        return route;
    }
}
