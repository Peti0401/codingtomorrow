package com.ibjava.codingtomorrow.logic.navigation;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;

/**
 * Basically a linked list node
 * to track back the path of the car from the destination to the start
 */
public class Step {

    private Position position;
    private Step previousStep;
    private Direction heading;

    public Step(Step step) {
        this.position = step.getPosition();
        this.previousStep = step.getPreviousStep();
        this.heading = step.getHeading();
    }

    Step(Position position) {
        this.position = new Position(position);
        this.previousStep = null;
        this.heading = null;
    }

    Step(Position position, Step previousStep) {
        this.position = new Position(position);
        this.previousStep = previousStep;
        setHeading(previousStep);
    }

    public Position getPosition() {
        return new Position(position);
    }

    public Step getPreviousStep() {
        return previousStep;
    }

    public Direction getHeading() {
        return heading;
    }

    public void setPosition(Position position) {
        this.position = new Position(position);
    }

    void setPreviousStep(Step previousStep) {
        this.previousStep = previousStep;
        setHeading(previousStep);
    }

    private void setHeading(Step previousStep) {
        this.heading = previousStep == null ? null : previousStep.getPosition().getDirectionTo(this.position);
    }

}