package com.ibjava.codingtomorrow.logic.navigation;

import com.ibjava.codingtomorrow.controlclasses.*;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.logic.analyser.MapAnalyser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ibjava.codingtomorrow.controlclasses.Response.NO_OP;

/**
 * a section represents a straight part of a route with starting end ending direction
 * commands are calculated based on sections control the car such that:
 *
 * (startDirection != endDirection) => it can turn at the end of the section, meaning that the speed of the car
 *                                     is going to be 1 on the field before the last (in the section)
 *
 * (startDirection == endDirection) => otherwise the commands are calculated so that the speed of the car is 1
 *                                     on the last field (in the section)
 */
public class Section {

    private final int steps;
    private final Position start;
    private final Position end;
    private final Direction startDirection;
    private final Direction endDirection;

    public Section(int steps, Position start, Position end, Direction startDirection, Direction endDirection) {
        this.steps = steps;
        this.start = new Position(start);
        this.end = new Position(end);
        this.startDirection = startDirection;
        this.endDirection = endDirection;
    }

    public static List<Section> generateSections(List<Position> route) {
        List<Section> sections = new ArrayList<>();

        if (route == null || route.size() < 2) {
            return sections;
        }

        Direction prev = Direction.getDirection(route.get(0), route.get(1));
        Position start = new Position(route.get(0));
        int sectionLength = 1;

        if (route.size() == 2) {
            Position end = route.get(1);
            Section section = new Section(sectionLength, start, end, prev, prev);
            sections.add(section);
        } else if (route.size() > 2) {
            for (int i = 1; i < route.size() - 1; i++) {
                Direction relativeRight = Direction.getRelativeDirection(prev, Direction.RIGHT);

                Position position_prev = route.get(i - 1);
                Position position_cur = route.get(i);
                Position position_next = route.get(i + 1);

                Direction direction_cur = Direction.getDirection(position_cur, position_next);

                int roadsToRight_prev = MapAnalyser.getRoadsToDirection(position_prev, relativeRight);
                int roadsToRight_cur = MapAnalyser.getRoadsToDirection(position_cur, relativeRight);
                int roadsToRight_next = MapAnalyser.getRoadsToDirection(position_next, relativeRight);

                if ( (prev != direction_cur)
                        || (roadsToRight_prev == 0 && roadsToRight_cur > 1 && !position_cur.isInRoundAbout())
                        || position_next.isZebra()
                        || position_next.isCarRailCross()) {

                    Section section = new Section(sectionLength, start, position_cur, prev, direction_cur);
                    sections.add(section);

                    start = new Position(position_cur);
                    sectionLength = 1;
                    prev = direction_cur;
                } else {
                    sectionLength++;
                }
            }

            Position end = new Position(route.get(route.size() - 1));
            Section section = new Section(sectionLength, start, end, prev, prev);
            sections.add(section);
        }

        return sections;
    }

    public static List<Position> generatePositions(List<Section> sections) {
        List<Position> positions = new ArrayList<>();

        for (Section section : sections) {
            for (int i = 0; i < section.getSteps(); i++) {
                Position position = section.getStart().translateToDirection(section.getStartDirection(), (byte) i);
                positions.add(position);
            }
        }

        positions.add(new Position(sections.get(sections.size() - 1).getEnd()));

        return positions;
    }

    public int getSteps() {
        return steps;
    }

    public Position getStart() {
        return start;
    }

    public Position getEnd() {
        return end;
    }

    public Direction getStartDirection() {
        return startDirection;
    }

    public Direction getEndDirection() {
        return endDirection;
    }

    public boolean isInSection(Position position) {
        Position examined = new Position(start);

        if (start.equals(end)) return start.equals(position);

        do {
            if (position.equals(examined)) return true;

            examined = examined.translateToDirection(startDirection, (byte) 1);
        } while (!examined.equals(end));

        return false;
    }

    public int remainingSteps(Position position) {
        for (int i = 0; i < steps; i++) {
            Position examined = start.translateToDirection(startDirection, (byte) i);

            if (examined.equals(position)) {
                return steps - i;
            }
        }

        System.err.println("no remaining step");
        return 0;
    }

    public int doneSteps(Position position) {
        for (int i = 0; i <= steps; i++) {
            Position examined = start.translateToDirection(startDirection, (byte) i);

            if (examined.equals(position)) {
                return i;
            }
        }

        System.err.println("no done step");
        return 0;
    }

    public boolean isSmallTurn() {
        return steps == 1 && startDirection != endDirection;
    }

    public boolean isBigTurn() {
        return steps == 2 && startDirection != endDirection;
    }

    public boolean isBigLeftTurn() {
        return steps == 2 && Direction.getRelativeDirection(startDirection, Direction.LEFT) == endDirection;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Section) {
            Section section = (Section) obj;
            return section.getStart().equals(start) && section.getEnd().equals(end);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Section{" + "\n" +
                "\tsteps=" + steps + "\n" +
                "\tstart=" + start + "\n" +
                "\tend=" + end + "\n" +
                "\tstartDirection=" + startDirection + "\n" +
                "\tendDirection=" + endDirection + "\n" +
                '}';
    }


    /**
     * retrieve the next command based on the cars ahead us (adaptive cruise) and the properties of our car
     *
     * @param tick the tick from which we retrieve the cars (they are in 1 tick delay)
     * @param car on which our command immediately applied (usually a simulated car object is passed)
     * @return list of commands for next move
     */
    public List<String> getCommandForSection(Tick tick, Car car) {
        List<String> commands = new ArrayList<>();

        int speed = car.getSpeed();
        int maxSpeed = car.getMaxSpeed();

        // all of the following methods deal with cars ahead of us
        boolean shouldTurn = AdaptiveCruise.shouldTurn(this, car);
        boolean shouldAccelerate = AdaptiveCruise.shouldAccelerate(this, car, tick);
        boolean shouldDecelerate = AdaptiveCruise.shouldDecelerate(this, tick.getCars(), car);


        // ================================
        // priority 1: we should decelerate
        // ================================
        if (shouldDecelerate) {
            // we have to slow down
            commands.add(Response.DECELERATION);
        }

        // ==========================
        // priority 2: we should turn
        // ==========================
        else if (shouldTurn) {
            // we have to turn
            // turning in motion
            if (Direction.getRelativeDirection(this.getStartDirection(), Direction.RIGHT) == this.getEndDirection()) {
                commands.add(Response.CAR_INDEX_RIGHT);
            } else if (Direction.getRelativeDirection(this.getStartDirection(), Direction.LEFT) == this.getEndDirection()) {
                commands.add(Response.CAR_INDEX_LEFT);
            } else {
                System.err.println("Car tries to turn more than 90 degrees inside a section");
            }
        }

        // =================================
        // priority 3: we should keep moving
        // =================================
        else if (!shouldAccelerate) {
            // we should keep moving with the same speed
            commands.add(NO_OP);
        }

        // ================================
        // priority 4: we should accelerate
        // ================================
        else if (speed < maxSpeed) {
            // we can accelerate
            commands.add(Response.ACCELERATION);
        } else if (speed == maxSpeed) {
            // we could accelerate, but the max speed doesn't let us
            commands.add(NO_OP);
        }

        // ====================
        // something went wrong
        // ====================
        else {
            // something went wrong
            System.err.println("error in converting section to commands");
            System.err.println("section: " + this);
            System.err.println("shouldTurn: " + shouldTurn);
            System.err.println("shouldAccelerate: " + shouldAccelerate);
            System.err.println("shouldDecelerate: " + shouldDecelerate);
        }

        return commands;
    }
}
