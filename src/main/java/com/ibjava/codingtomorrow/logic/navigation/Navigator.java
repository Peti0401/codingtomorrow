package com.ibjava.codingtomorrow.logic.navigation;

import com.ibjava.codingtomorrow.controlclasses.*;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.entities.Pedestrian;
import com.ibjava.codingtomorrow.logic.analyser.MapAnalyser;
import com.ibjava.codingtomorrow.logic.route.Route;
import com.ibjava.codingtomorrow.logic.route.RouteFactory;
import com.ibjava.codingtomorrow.logic.simulator.Simulator;
import sun.misc.Queue;

import java.util.*;

import static com.ibjava.codingtomorrow.controlclasses.Response.*;

public class Navigator {

    private static List<String> sentCommands = new ArrayList<>();
    private static String previousCommand = null;

    private static Queue<String> commandQueue = new Queue<>();

    // current route
    private static Route route = null;

    static Section prevSection = null;
    static Section currentSection = null;
    static int passenger_id = 0;

    private static Car previousCar = null;

    private static int beenInOnePlace = 0;

    public static void init() {
        sentCommands.clear();
        previousCommand = null;

        prevSection = null;
        currentSection = null;

        beenInOnePlace = 0;
        route = null;
    }

    private static String sendCommand(String command) {
        sentCommands.add(command);
        previousCommand = command;
        return command;
    }

    public static String getCommand(Tick tick) {
        // tick variables ==============================
        Car[] cars = tick.getCars();
        Car ourCar = tick.getOurCar();
        Passenger[] passengers = tick.getPassengers();
        Pedestrian[] pedestrians = tick.getPedestrians();
        // =============================================

        // checking for staying in one place
        if (previousCar.getPos().equals(ourCar.getPos())) {
            beenInOnePlace++;
        } else {
            beenInOnePlace = 0;
        }

        if (hadCrash(ourCar)) {
            route = null;
            commandQueue = new Queue<>();
            previousCommand = null;

            System.err.println("============================================");
            System.err.println("========== car crashed, rerouting ==========");
            System.err.println("============================================");
        }

        Car ourSimulatedCar = Simulator.simulateCommand(previousCommand, tick);

        if (ourSimulatedCar.hasPassenger() && ourSimulatedCar.getPassenger_id() != passenger_id) {
            // there were two passengers on the same location and we thought we picked up another
            // -> so the calculated destination is not for the current passenger
            route = null;
        }

        if (route != null
                && !route.isFinished(ourSimulatedCar)
                && Arrays.stream(passengers).noneMatch(passenger -> passenger.getId() == route.getPassenger_id())) {
            // someone picked up the passenger that we were going for
            route = null;
        }

        // starting directions
        List<Direction> possibleSTartingDirections = new ArrayList<>();
        if (beenInOnePlace > 7) {
            for (Direction dir : Direction.values()) {
                if (dir != ourCar.getDirection()) possibleSTartingDirections.add(dir);
            }
            route = null;
        } else {
            possibleSTartingDirections.addAll(Arrays.asList(Direction.values()));
        }

        if (route == null || route.isFinished(ourSimulatedCar)) {

            if (ourSimulatedCar.hasPassenger()) {
                // calculate route to drop the current passenger
                System.err.println("ROUTE TO DROP");

                passenger_id = ourSimulatedCar.getPassenger_id();

                route = RouteFactory.routeToDropPassenger(ourSimulatedCar, passengers, possibleSTartingDirections);
            } else {
                // calculate route to pick up next passenger
                System.err.println("ROUTE TO PICK UP");

//                route = RouteFactory.routeToPickUpSafestPassenger(ourSimulatedCar, passengers, passenger_id);
                route = RouteFactory.routeToPickUpClosestPassenger(ourSimulatedCar, passengers, passenger_id, possibleSTartingDirections);
            }

            route.getSections().forEach(System.out::println);

        }



        currentSection = route.getCurrentSection(ourSimulatedCar);

        // debug message ===================
        if (prevSection == null || !prevSection.equals(currentSection)) System.err.println("current currentSection: " + currentSection);
        prevSection = currentSection;

        System.out.println("ticks car: " + tick.getOurCar());
        System.out.println("simulated car: " + ourSimulatedCar);
        // =================================

        Direction carDirection = ourSimulatedCar.getDirection();

        if (currentSection == null) {
            // the car should slow down to 0 speed, to drop the passenger
            commandQueue.enqueue(Response.DECELERATION);
        } else {

//            if (Sensor.shouldStop_carFromRight(cars, ourCar)) {
//                commandQueue.enqueue(CLEAR);
//            }

            // ===================
            // giving way to train
            // ===================
            if (MapAnalyser.isEnteringRailCross(ourSimulatedCar)) {

                System.out.println("entering rail cross");
                if (MapAnalyser.isEnteringZebra(ourSimulatedCar, 2)) {
                    // there is a zebra right after the rails
                    // we should  be extra careful

                    if (Sensor.shouldStopBeforeZebra(pedestrians, ourSimulatedCar, 2)) {
                        System.out.println("zebra with potential pedestrian -> stop");
                        // stop the car
                        commandQueue.enqueue(getStoppingCommand(ourSimulatedCar));
                    } else {
                        // check for train
                        if (Sensor.shouldStopBeforeRailCross(ourSimulatedCar, tick.getRequest_id().getTick() + 1)) {
                            // stop the car
                            commandQueue.enqueue(getStoppingCommand(ourSimulatedCar));
                        }
                    }


                } else {
                    // there is no zebra right after the rails
                    if (Sensor.shouldStopBeforeRailCross(ourSimulatedCar, tick.getRequest_id().getTick() + 1)) {
                        // stop the car
                        commandQueue.enqueue(getStoppingCommand(ourSimulatedCar));
                    }
                }

            }

            // ============================
            // giving way to car from right
            // ============================
            if (MapAnalyser.isCrossingDisjunction(ourSimulatedCar, currentSection)) {
                // when this point reached the speed is 1
                if (Sensor.shouldStop_carFromRight(cars, ourSimulatedCar)) {
                    // stop the car
                    commandQueue.enqueue(getStoppingCommand(ourSimulatedCar));
                }
            }

            // ================================
            // giving way to cars in roundabout
            // ================================
            if (MapAnalyser.isEnteringRoundabout(ourSimulatedCar)) {
                // roundabout incoming
                // when this point reached the speed is 1

                if (Sensor.shouldStopBeforeRoundabout(cars, ourSimulatedCar)) {
                    // stop the car
                    commandQueue.enqueue(getStoppingCommand(ourSimulatedCar));
                }
            }

            // ===================================
            // giving way to pedestrians  on zebra
            // ===================================
            if (MapAnalyser.isEnteringZebra(ourSimulatedCar, 1)) {
                // when this point reached the speed is 1

                if (Sensor.shouldStopBeforeZebra(pedestrians, ourSimulatedCar, 1)) {
                    // stop the car
                    commandQueue.enqueue(getStoppingCommand(ourSimulatedCar));
                }
            }

            if (commandQueue.isEmpty()) {
                // we can give commands
                if (currentSection.isInSection(ourSimulatedCar.getPos())) {
                    if (currentSection.getStartDirection() == carDirection) {
                        // we are on the currentSection
                        // we can go or turn in motion
                        List<String> commands = currentSection.getCommandForSection(tick, ourSimulatedCar);
                        commands.forEach(command -> commandQueue.enqueue(command));

                        commands.forEach(System.out::println);

                    } else if (currentSection.getStartDirection() != carDirection) {
                        // we are on the currentSection
                        // we have to turn on spot
                        if (ourSimulatedCar.getSpeed() > 0) {
                            commandQueue.enqueue(DECELERATION);
                        }

                        List<String> turnToRightDirection =
                                new ArrayList<>(Arrays.asList(Direction.changeDirectionTo(carDirection, currentSection.getStartDirection())));
                        turnToRightDirection.forEach(command -> commandQueue.enqueue(command));

                        turnToRightDirection.forEach(System.out::println);

                    }
                } else {
                    // something happened
                    Navigator.init();
                    System.err.println("============================================");
                    System.err.println("============ restart navigation ============");
                    System.err.println("============================================");
                    return getCommand(tick);
                }
            }
        }

        String command; // next command
        try {
            command = commandQueue.dequeue();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("============================================");
            System.err.println("======== no command was calculated =========");
            System.err.println("============================================");
            command = NO_OP;
        }

        // save car state
        previousCar = new Car(ourCar);

        return sendCommand(command);
    }

    private static boolean hadCrash(Car currentCar) {
        if (previousCar != null) {
            if (previousCar.getLife() - currentCar.getLife() >= 15) {
                return true;
            }
        }

        return false;
    }

    private static String getStoppingCommand(Car ourCar) {
        if (ourCar.getSpeed() > 0) {
            return DECELERATION;
        } else {
            return NO_OP;
        }
    }
}
