package com.ibjava.codingtomorrow.logic.navigation;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.entities.Car;

/**
 *
 */
public class AdaptiveCruise {

    public static final int SIGH_FORWARD_FIELDS = 7; // number of fields that our car senses forward

    // ======================= START ===============================
    // ===================== accelerate ============================

    /**
     *
     * @param section the currentSection that the car is on
     * @param car the car !on which the command is immediately applied! (usually simulated car)
     * @param tick the tick we want to examine
     * @return whether or not we can accelerate
     */
    public static boolean shouldAccelerate(Section section, Car car, Tick tick) {

        int fieldsForward = getNumberOfFieldsForward(section, tick.getCars(), car);

        return shouldAccelerate(car.getSpeed(), fieldsForward);
    }

    /**
     *
     * @param speed the current speed of the car
     * @param remainingFields we have that much steps to slow down to 1
     * @return
     */
    public static boolean shouldAccelerate(int speed, int remainingFields) {

        boolean canAccelerate = false;

        switch (speed) {
            case 2:
                canAccelerate = remainingFields >= 7;
                break;
            case 1:
                canAccelerate = remainingFields >= 4;
                break;
            case 0:
                canAccelerate = remainingFields >= 1;
                break;
        }

        return canAccelerate;
    }

    // ===================== accelerate ============================
    // ======================== END ================================

    // ======================= START ===============================
    // ===================== decelerate ============================

    /**
     *
     * @param section the currentSection that the car is on
     * @param ourCar the car !on which the command is immediately applied! (usually simulated car)
     * @return whether or not we should decelerate
     */
    public static boolean shouldDecelerate(Section section, Car[] cars, Car ourCar) {

        if (section.getStartDirection() != section.getEndDirection()
                && section.remainingSteps(ourCar.getPos()) == 1
                && getNumberOfFieldsToCarForward(cars, ourCar) == 0) {
            // if we should turn
            return false;
        }

        int fieldsForward = getNumberOfFieldsForward(section, cars, ourCar);

        return shouldDecelerate(ourCar.getSpeed(), fieldsForward);
    }

    /**
     *
     * @param speed the current speed of the car
     * @param remainingFields we have that much steps to slow down to 1
     * @return
     */
    public static boolean shouldDecelerate(int speed, int remainingFields) {

        boolean shouldDecelerate = false;

        switch (speed) {
            case 3:
                shouldDecelerate = remainingFields <= 6;
                break;
            case 2:
                shouldDecelerate = remainingFields <= 3;
                break;
            case 1:
                shouldDecelerate = remainingFields <= 1;
                break;
        }

        return shouldDecelerate;
    }

    public static boolean canDecelerateSafely(int speed,  int fieldsAhead) {
        boolean canStopSafely = true;

        switch (speed) {
            case 3:
                canStopSafely = fieldsAhead > 4;
                break;
            case 2:
                canStopSafely = fieldsAhead > 1;
                break;
            case 1:
//                canStopSafely = fieldsAhead > 1;
                break;
        }

        return canStopSafely;
    }

    // ===================== decelerate ============================
    // ======================== END ================================

    // ======================= START ===============================
    // ======================== turn ===============================

    public static boolean shouldTurn(Section section, Car car) {
        int steps = section.getSteps();
        int place = section.doneSteps(car.getPos());

        return shouldTurn(place, car.getSpeed(), steps, section.getStartDirection(), section.getEndDirection());
    }

    /**
     *
     * @param place the index of car in the currentSection
     * @param speed the current speed of the car
     * @param steps the steps has to be done in the current currentSection
     * @param startDirection starting direction of the currentSection
     * @param endDirection ending direction of the current currentSection (starting direction of the next currentSection)
     * @return true if we should index, based on our place and speed in the currentSection
     */
    public static boolean shouldTurn(int place, int speed, int steps, Direction startDirection, Direction endDirection) {
        boolean shouldTurn = false;

        if (startDirection == endDirection) {
            return false;
        }

        int stepsAhead = steps - place;

        switch (speed) {
            case 3:
                shouldTurn = stepsAhead == 2;
                break;
            case 2:
            case 1:
                shouldTurn = stepsAhead == 1;
                break;
            case 0:
                shouldTurn = stepsAhead == 0;
                break;
        }

        return shouldTurn;
    }

    // ======================== turn ===============================
    // ======================== END ================================

    /**
     * returns the number of fields we have to step forward to reach the car ahead of us
     * @param cars all the cars in current tick
     * @param ourCar our car
     * @return number of fields
     */
    public static int getNumberOfFieldsToCarForward(Car[] cars, Car ourCar) {

        for (int i = 1; i <= SIGH_FORWARD_FIELDS; i++) {
            Position examined = ourCar.getPos()
                    .translateToDirection(ourCar.getDirection(), (byte) i);

            if (!examined.isRoad()) break;

            for (Car car : cars) {
                if (car.getPos().equals(examined)) {
                    return i + Math.max(car.getSpeed() - 1, 0); // -1 (worst case scenario, it slows down)
                }
            }
        }

        return 0;
    }

    public static int getNumberOfFieldsForward(Section section, Car[] cars, Car ourCar) {
        int fieldsToEndOfSection = section.remainingSteps(ourCar.getPos());
        if (section.getStartDirection() == section.getEndDirection()) fieldsToEndOfSection += 1;

        int fieldsToCarForward = getNumberOfFieldsToCarForward(cars, ourCar);

        // if there is a car ahead, changes the length of this currentSection to match the steps till car
        return (fieldsToCarForward > 0 && fieldsToCarForward < fieldsToEndOfSection) ? fieldsToCarForward : fieldsToEndOfSection;
    }
}
