package com.ibjava.codingtomorrow.logic.navigation;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.entities.Car;
import com.ibjava.codingtomorrow.entities.Pedestrian;
import com.ibjava.codingtomorrow.entities.Train;
import com.ibjava.codingtomorrow.logic.analyser.MapAnalyser;
import com.ibjava.codingtomorrow.logic.simulator.Simulator;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * sensor class contains static methods to make decisions based on the environment
 */
class Sensor {

    // ================== giving way logic =========================

    /**
     *  CRITICAL POSITIONS:
     *
     *      pos1             pos2             pos3
     *      ::::    ::::  |  ::::    ::::  |  ::::    ::::
     *      ::        ::  |  ::        ::  |  ::        ::
     *          ::::      |      ::::      |      ::::
     *          ::::      |      ::::      |      ::::
     *      ::x>      ::  |  ::  x>    ::  |  ::    x>  ::
     *      ::::  /\::::  |  ::::  /\::::  |  ::::  /\::::
     *
     * @param cars the cars that are present
     * @param ourCar our car when entering the roundabout
     * @return whether or not we should stop to give way
     */
    public static boolean shouldStopBeforeRoundabout(Car[] cars, Car ourCar) {
        Direction carDirection = ourCar.getDirection();
        Direction relativeLeft = Direction.getRelativeDirection(carDirection, Direction.LEFT);

        // change the critical point to examine the right place
        Position pos1 = ourCar.getPos()
                .translateToDirection(carDirection, (byte) 1)
                .translateToDirection(relativeLeft, (byte) 2);

        Position pos2 = ourCar.getPos()
                .translateToDirection(carDirection, (byte) 1)
                .translateToDirection(relativeLeft, (byte) 1);

        Position pos3 = ourCar.getPos()
                .translateToDirection(carDirection, (byte) 1);

        for (Car car : cars) {
            if ( (car.getPos().equals(pos1) && (car.getSpeed() > 0 || ourCar.getSpeed() == 0) )
                    || (car.getPos().equals(pos2) && (car.getSpeed() > 0 || ourCar.getSpeed() == 0) )
                    || (car.getPos().equals(pos3) && car.getSpeed() <= 1)) {
                System.err.println("GIVING WAY TO CAR IN THE ROUNDABOUT");
                return true;
            }
        }

        return false;
    }

    /**
     *
     *
     *
     *
     * @param pedestrians
     * @param ourCar
     * @param tickTillZebra
     * @return
     */
    public static boolean shouldStopBeforeZebra(Pedestrian[] pedestrians, Car ourCar, int tickTillZebra) {
        List<Pedestrian> simulatedPedestrians = new ArrayList<>(Arrays.asList(pedestrians));

        for (int i = 0; i < tickTillZebra; i++) {
            simulatedPedestrians = Simulator.simulatePedestrians(simulatedPedestrians.toArray(new Pedestrian[0]));
        }

        Direction carDirection = ourCar.getDirection();

        Direction relativeLeft = Direction.getRelativeDirection(carDirection, Direction.LEFT);
        Direction relativeRight = Direction.getRelativeDirection(carDirection, Direction.RIGHT);

        Position leftCriticalPoint = ourCar.getPos()
                .translateToDirection(carDirection, (byte) tickTillZebra)  //  change one to ticktillzebra
                .translateToDirection(relativeLeft, (byte) 1);

        Position leftCriticalPoint2 = ourCar.getPos()
                .translateToDirection(carDirection, (byte) tickTillZebra)
                .translateToDirection(relativeLeft, (byte) 2);

        Position rightCriticalPoint = ourCar.getPos()
                .translateToDirection(carDirection, (byte) tickTillZebra)
                .translateToDirection(relativeRight, (byte) 1);

        Position aheadCriticalPoint = ourCar.getPos()
                .translateToDirection(carDirection, (byte) tickTillZebra);

        for (Pedestrian simulatedPedestrian : simulatedPedestrians) {
            if ( simulatedPedestrian.getPos().equals(leftCriticalPoint)
//                    && simulatedPedestrian.getDirection() == relativeRight)
                    || simulatedPedestrian.getPos().equals(rightCriticalPoint)
                    || simulatedPedestrian.getPos().equals(aheadCriticalPoint)
                    || (simulatedPedestrian.getPos().equals(leftCriticalPoint2) && ourCar.getSpeed() == 0)) {
                System.err.println("GIVING WAY TO PEDESTRIAN ON ZEBRA");
                return true;
            }
        }

        return false;
    }

    /**
     *      pos1             pos2             pos3             pos4               pos5 (out of sight)
     *        ::    ::    |    ::    ::    |    ::    ::    |    ::    ::      |    ::    ::
     *      ::::    ::::  |  ::::    ::::  |  ::::    ::::  |  ::::    ::::::  |  ::::    ::::::::
     *            <x      |          <x    |            <x  |              <x  |                <x
     *            /\      |        /\      |        /\      |        /\        |        /\
     *      ::::    ::::  |  ::::    ::::  |  ::::    ::::  |  ::::    ::::::  |  ::::    ::::::::
     *        ::    ::    |    ::    ::    |    ::    ::    |    ::    ::      |    ::    ::
     *
     * @return
     */
    public static boolean shouldStop_carFromRight(@NotNull Car[] cars, @NotNull Car ourCar) {
        Direction ourDirection = ourCar.getDirection();
        Direction relativeRight = Direction.getRelativeDirection(ourDirection, Direction.RIGHT);
        Direction relativeLeft = Direction.getRelativeDirection(ourDirection, Direction.LEFT);

        Position pos1 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1);

        Position pos2 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1)
                .translateToDirection(relativeRight, (byte) 1);

        Position pos3 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1)
                .translateToDirection(relativeRight, (byte) 2);

        Position pos4 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1)
                .translateToDirection(relativeRight, (byte) 3);

        for (Car car : cars) {
            Position carPosition = car.getPos();
            if ( (carPosition.equals(pos1) && car.getSpeed() <= 1)
                    || (carPosition.equals(pos2) && car.getDirection() == relativeLeft)
                    || (carPosition.equals(pos3) && car.getDirection() == relativeLeft)
                    || (carPosition.equals(pos4) && car.getDirection() == relativeLeft) ) {
                System.err.println("GIVING WAY TO CAR FROM RIGHT");
                return true;
            }
        }

        return false;
    }

    /**
     *      pos1             pos2             pos3             pos4               pos5 (out of sight)
     *        ::    ::    |    ::    ::    |    ::    ::    |    ::    ::      |    ::    ::
     *      ::::    ::::  |  ::::    ::::  |  ::::    ::::  |  ::::    ::::::  |  ::::    ::::::::
     *            <x      |          <x    |            <x  |              <x  |                <x
     *            /\      |        /\      |        /\      |        /\        |        /\
     *      ::::    ::::  |  ::::    ::::  |  ::::    ::::  |  ::::    ::::::  |  ::::    ::::::::
     *        ::    ::    |    ::    ::    |    ::    ::    |    ::    ::      |    ::    ::
     *
     * @return
     */
    public static boolean shouldEmergencyStop_carFromRight(Car[] cars, Car ourCar) {
        Direction ourDirection = ourCar.getDirection();
        Direction relativeRight = Direction.getRelativeDirection(ourDirection, Direction.RIGHT);
        Direction relativeLeft = Direction.getRelativeDirection(ourDirection, Direction.LEFT);

        Position pos1 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1);

        Position pos2 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1)
                .translateToDirection(relativeRight, (byte) 1);

        Position pos3 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1)
                .translateToDirection(relativeRight, (byte) 2);

        Position pos4 = ourCar.getPos()
                .translateToDirection(ourDirection, (byte) 1)
                .translateToDirection(relativeRight, (byte) 3);

        for (Car car : cars) {
            Position carPosition = car.getPos();
            if ( (carPosition.equals(pos1) && car.getSpeed() <= 1)
                    || (carPosition.equals(pos2) && car.getDirection() == relativeLeft)
                    || (carPosition.equals(pos3) && car.getDirection() == relativeLeft)
                    || (carPosition.equals(pos4) && car.getDirection() == relativeLeft) ) {
                if (ourCar.getSpeed() > 0) {
                    System.err.println("GIVING EMERGENCY WAY TO CAR FROM RIGHT");
                    return true;
                }
            }
        }

        return false;
    }



    /**
     *  end of the train is ahead of us:
     *
     *    scenario 1.:     |   scenario 2.:
     *                     |
     *        ->           |        ->  _ _ _
     *      □□□□...        |     ...□□□□ _ _ _
     *      ↑              |                 ↑
     *
     *
     * @param car
     * @param tickIndex
     * @return
     */
    public static boolean shouldStopBeforeRailCross(Car car, int tickIndex) {
        Position crossPosition = car.getPos()
                .translateToDirection(car.getDirection(), (byte) 1);

        Train train = MapAnalyser.getCrossingTrain(crossPosition);

        Direction trainHeading = train.getDirection();

        Position trainTail = train.getTail(tickIndex);
        Position trainHead = train.getHead(tickIndex);

        if (trainTail == null && trainHead == null) return false; // the train is not on the board

        // scenario 1.
        boolean scenario_1 = crossPosition.equals(trainTail)
                || crossPosition.getDirectionToNotAdjacentPosition(trainTail).contains(trainHeading);

        // scenario 2.
        int steps = 0;
        Direction inverse = trainHeading.getInverse(); // direction towards train (inverse of the train's direction)
        if (crossPosition.getDirectionToNotAdjacentPosition(trainHead).contains(inverse)) {
            while (!crossPosition.translateToDirection(inverse, (byte) steps).equals(trainHead)) {
                steps++;
            }
        }

        boolean scenario_2 = steps >= 6;


        boolean shouldStop = !(scenario_1 || scenario_2);

        System.out.println("Train: " + train.getCompassPoint());
        System.out.println("scenario 1: " + scenario_1);
        System.out.println("scenario 2: " + scenario_2);
        if (shouldStop) System.err.println("GIVING WAY TO TRAIN ON CROSSING");

        return shouldStop;
    }
}
