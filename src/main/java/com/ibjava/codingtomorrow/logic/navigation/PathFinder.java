package com.ibjava.codingtomorrow.logic.navigation;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.entities.Passenger;
import com.ibjava.codingtomorrow.logic.analyser.MapAnalyser;
import org.jetbrains.annotations.Nullable;
import sun.misc.Queue;

import java.util.*;

/**
 * class to store path finding related static methods
 */
public class PathFinder {

    private static final byte MAX_STEPS_ON_GRASS = 10;
    private static final boolean KEEP_TO_THE_RIGHT = true; // constant to enable/disable keep-to-right logic

    private static final Map<Position, Direction> RESTRICTIONS = new HashMap<Position, Direction>(){
        {
            put(new Position(50, 10), Direction.DOWN);
            put(new Position(52, 11), Direction.UP);
            put(new Position(51, 14), Direction.RIGHT);
            put(new Position(50, 12), Direction.DOWN);

            put(new Position(32, 15), Direction.LEFT);
            put(new Position(35, 15), Direction.LEFT);
            put(new Position(30, 16), Direction.DOWN);
            put(new Position(34, 17), Direction.RIGHT);
        }
    };

    // ======================== START ==============================
    // =================== getPathToRoad() =========================

    /**
     * returns the possible ways to the adjacent road
     * @param from the starting position
     * @return list of endPoints on the road
     */
    private static List<Step> getPathToRoad(Position from) {
        return getPathToRoad(from, Direction.values());
    }

    /**
     * plans all possible routes from a position to road fields within the range of maximum steps
     * @param from the position where we start
     * @param heading the possible starting directions (depends on car direction)
     * @return list of endPoints on the road
     */
    private static List<Step> getPathToRoad(Position from, Direction... heading) {
        return getPathToRoad(from, MAX_STEPS_ON_GRASS, heading);
    }

    /**
     * plans all possible routes from a position to road fields
     * @param from the position where we start
     * @param maxStepsOnGrass the maximum number of steps on grass
     * @param heading the possible starting directions (depends on car direction)
     * @return a list of endPoints on the road
     */
    private static List<Step> getPathToRoad(Position from, byte maxStepsOnGrass, Direction... heading) {
        if (from.isRoad()) {
            return Arrays.asList(new Step(from));
        } else if (from.isObstacle()) {
            System.out.println("From position: " + from + " is an obstacle");
            return null;
        }

        List<Step> possibleOutcomes = new ArrayList<>(); // the final steps to the road
        Queue<Step> steps = new Queue<>();
        ArrayList<Position> occupiedPositions = new ArrayList<>();

        Step currentStep = new Step(from);
        steps.enqueue(currentStep);
        occupiedPositions.add(currentStep.getPosition());

        while (!steps.isEmpty()) {

            try {
                currentStep = steps.dequeue();
            } catch (InterruptedException e) {
                // there is no more step in the queue
                e.printStackTrace();
                break;
            }

            if (currentStep.getPosition().isRoad()) {
                // we have reached a road
                possibleOutcomes.add(currentStep);
            } else if (maxStepsOnGrass == getLengthOfPath(currentStep)) {
                // we have reached the step limit on grass
            } else {
                // we can check the possible next steps

                for (Direction direction : Direction.values()) {
                    if (currentStep.getPosition().equals(from) && !Arrays.asList(heading).contains(direction)) {
                        continue;
                    }

                    Step newStep = new Step(
                            currentStep.getPosition().translateToDirection(direction, (byte) 1),
                            currentStep);

                    if (occupiedPositions.contains(newStep.getPosition()) || newStep.getPosition().isObstacle()) {
                        // discard the new step
                        newStep.setPreviousStep(null);
                    } else {
                        // keep new step
                        steps.enqueue(newStep);
                        occupiedPositions.add(newStep.getPosition());
                    }
                }
            }
        }
        return possibleOutcomes;
    }

    /**
     * @param step end node of the path linked list
     * @return length of a path linked to the parameter step
     */
    private static byte getLengthOfPath(Step step) {
        return step == null ? 1 : (byte) (1 + getLengthOfPath(step.getPreviousStep()));
    }

    // =================== getPathToRoad() =========================
    // ======================== END ================================

    // ======================== START ==============================
    // ================ getPathToNearestRoad() =====================

    /**
     * calculates the shortest route from a field to the road
     * @param from the position where we start
     * @return the final step on the road
     */
    private static Step getPathToNearestRoad(Position from, Passenger[] passengers) {
        return getPathToNearestRoad(from, passengers, Direction.values());
    }

    /**
     * calculates the shortest route from a field to the road
     * @param from the position where we start
     * @param heading the possible starting directions (depends on car direction)
     * @return the final step on the road
     */
    private static Step getPathToNearestRoad(Position from, Passenger[] passengers, Direction... heading) {

        if (from.isRoad()) {
            return new Step(from);
        } else if (from.isObstacle()) {
            System.out.println("From position: " + from + "is a building");
            return null;
        }

        Queue<Step> steps = new Queue<>();
        ArrayList<Position> occupiedPositions = new ArrayList<>();

        Step currentStep = new Step(from);
        steps.enqueue(currentStep);
        occupiedPositions.add(currentStep.getPosition());

        while (!currentStep.getPosition().isRoad()) {

            try {
                currentStep = steps.dequeue();
            } catch (InterruptedException e) {
                // there is no more step in the queue
                e.printStackTrace();
                break;
            }

            for (Direction direction : Direction.values()) {
                if (currentStep.getPosition().equals(from) && !Arrays.asList(heading).contains(direction)) {
                    continue;
                }

                Step newStep = new Step(
                        currentStep.getPosition().translateToDirection(direction, (byte) 1),
                        currentStep);

                if (occupiedPositions.contains(newStep.getPosition())
                        || newStep.getPosition().isObstacle()
                        || Arrays.stream(passengers).anyMatch(passenger -> passenger.getPos().equals(newStep.getPosition()))) {
                    // discard the new step
                    newStep.setPreviousStep(null);
                } else {
                    // keep new step
                    steps.enqueue(newStep);
                    occupiedPositions.add(newStep.getPosition());
                }
            }

        }

        return currentStep;
    }

    // ================ getPathToNearestRoad() =====================
    // ======================== END ================================

    // ======================== START ==============================
    // =============== getPathFromRoadToRoad() =====================

    /**
     * calculates the route between two points of the road
     * @param from starting position
     * @param to destination
     * @return final step
     */
    private static Step getPathFromRoadToRoad(Position from, Position to, boolean keepToTheRight) {
        return getPathFromRoadToRoad(from, to, keepToTheRight, Direction.values());
    }

    /**
     * Returns the path between a starting and ending position.
     * @param from start position
     * @param to end position
     * @param heading the possible starting directions (depends on car direction)
     * @return final step
     */
    private static Step getPathFromRoadToRoad(Position from, Position to, boolean keepToTheRight, Direction... heading) {
        if (!from.isRoad() && !to.isRoad())  {
            System.out.println("error in: " + PathFinder.class.getSimpleName()  + ", in method: getPathFromRoadToRoad");
            return null;
        }

        Step currentStep = new Step(from);

        Queue<Step> tiles = new Queue<>();
        ArrayList<Position> occupiedPositions = new ArrayList<>();

        tiles.enqueue(currentStep);
        occupiedPositions.add(currentStep.getPosition());

        while (!currentStep.getPosition().equals(to)) {

            if (!tiles.isEmpty()) {
                try {
                    currentStep = tiles.dequeue();
                } catch (InterruptedException e) {
                    // there are no more steps in the queue
                    e.printStackTrace();
                }
            } else {
                break;
            }

            // check steps for each direction
            Direction[] relativeDirections = new Direction[]{Direction.RIGHT, Direction.UP, Direction.LEFT, Direction.DOWN};
            for (Direction direction : relativeDirections) {
                if (currentStep.getPosition().equals(from) && !Arrays.asList(heading).contains(direction)) {
                    continue;
                }

                Direction trueDirection = Direction.getRelativeDirection(currentStep.getHeading(), direction);

                Step newStep = new Step(
                        currentStep.getPosition().translateToDirection(trueDirection, (byte) 1), currentStep);

                Position newPosition = newStep.getPosition();

                if  (!occupiedPositions.contains(newPosition) && newPosition.isRoad()) {

                    Direction[] roundaboutDirection = MapAnalyser.getPossibleDirectionInRoundabout(newStep.getPosition());

                    Direction restrictedDirection = getRestrictedDirection(currentStep.getPosition());
                    if (keepToTheRight && restrictedDirection != null) {
                        if (restrictedDirection == newStep.getHeading()) {
                            // we are allowed to go that direction
                            tiles.enqueue(newStep);
                            occupiedPositions.add(newStep.getPosition());
                        } else {
                            // discard new step
                            newStep.setPreviousStep(null);
                        }
                    } else if ( !keepToTheRight
                            || (roundaboutDirection != null && Arrays.asList(roundaboutDirection).contains(trueDirection)) // we are in a roundabout
                            || (roundaboutDirection == null && MapAnalyser.isOnTheRightSide(newStep)) ) {
                        // we are allowed to go that direction
                        tiles.enqueue(newStep);
                        occupiedPositions.add(newStep.getPosition());
                    } else {
                        // discard new step
                        newStep.setPreviousStep(null);
                    }
                }
            }
        }

        if (to.equals(currentStep.getPosition())) {
            System.out.println("return  current step");
            return currentStep;
        } else {
            System.out.println("return  null");
            return null;
        }
    }

    @Nullable
    private static Direction getRestrictedDirection(Position position) {
        for (Map.Entry<Position, Direction> entry : RESTRICTIONS.entrySet()) {
            Position positionEntry = entry.getKey();
            Direction directionEntry = entry.getValue();

            if (positionEntry.equals(position)) return directionEntry;
        }

        return null;
    }

    // =============== getPathFromRoadToRoad() =====================
    // ======================== END ================================

    // ======================== START ==============================
    // ====================== getPath() ============================

    /**
     * calculates the route between any two points (must not be building coordinates)
     * @param from starting position
     * @param to destination
     * @return the route in the form of an array of positions
     */
    public static Position[] getPath(Position from, Position to, Passenger[] passengers) {
        return getPath(from, to, passengers, Direction.values());
    }

    /**
     * calculates the route between any two points (must not be building coordinates)
     * @param from starting position
     * @param to destination
     * @param heading the possible starting directions (depends on car direction)
     * @return the route in the form of an array of positions
     */
    public static Position[] getPath(Position from, Position to, Passenger[] passengers, Direction... heading) {
        List<Position> positions = new ArrayList<>();

        Step from_road = getPathToNearestRoad(from, passengers, heading);
        Step to_road = getPathToNearestRoad(to, passengers);
        Step fromRoad_toRoad = getPathFromRoadToRoad(from_road.getPosition(), to_road.getPosition(), KEEP_TO_THE_RIGHT);

        if (fromRoad_toRoad == null) {
            // if there is no possible route keeping to the right
            fromRoad_toRoad = getPathFromRoadToRoad(from_road.getPosition(), to_road.getPosition(), false);
        }

        // from ---> fromRoad
        List<Position> section = new ArrayList<>();
        while (from_road != null) {
            section.add(from_road.getPosition());
            from_road = from_road.getPreviousStep();
        }
        Collections.reverse(section);
        positions.addAll(section);

        // fromRoad ----> toRoad
        section = new ArrayList<>();
        while (fromRoad_toRoad != null) {
            section.add(fromRoad_toRoad.getPosition());
            fromRoad_toRoad = fromRoad_toRoad.getPreviousStep();
        }
        Collections.reverse(section);
        positions.addAll(section);

        // toRoad -----> to
        section = new ArrayList<>();
        while (to_road != null) {
            section.add(to_road.getPosition());
            to_road = to_road.getPreviousStep();
        }
        // no need for reverse
        positions.addAll(section);

        for (int i = 0; i < positions.size() - 1; i++) {
            Position cur = positions.get(i);
            Position next = positions.get(i+1);

            // remove duplicate
            if (cur.equals(next)) {
                positions.remove(i+1);
                i--;
            }
        }

        return positions.toArray(new Position[0]);
    }

    public static Position[] getPath_escape(Position from, Position to, Passenger[] passengers, Direction... heading) {

        Step to_road = getPathToNearestRoad(to, passengers);
        Step fromRoad_toRoad = getPathFromRoadToRoad(from, to_road.getPosition(), KEEP_TO_THE_RIGHT, heading);

        if (fromRoad_toRoad == null) {
            System.out.println("road was null");
            // if there is no possible route keeping to the right
            fromRoad_toRoad = getPathFromRoadToRoad(from, to_road.getPosition(), false);
        }

        List<Position> section = new ArrayList<>();

        // fromRoad ----> toRoad
        while (fromRoad_toRoad != null) {
            section.add(fromRoad_toRoad.getPosition());
            fromRoad_toRoad = fromRoad_toRoad.getPreviousStep();
        }
        Collections.reverse(section);
        List<Position> positions = new ArrayList<>(section);

        // toRoad -----> to
        section = new ArrayList<>();
        while (to_road != null) {
            section.add(to_road.getPosition());
            to_road = to_road.getPreviousStep();
        }
        // no need for reverse
        positions.addAll(section);

        for (int i = 0; i < positions.size() - 1; i++) {
            Position cur = positions.get(i);
            Position next = positions.get(i+1);

            // remove duplicate
            if (cur.equals(next)) {
                positions.remove(i+1);
                i--;
            }
        }

        return positions.toArray(new Position[0]);
    }

    // ====================== getPath() ============================
    // ======================== END ================================


    /**
     * @param middlePosition position in the middle
     * @return the adjacent road if there is one, otherwise returns the adjacent not obstacle position
     */
    public static Position getAdjacentRoad(Position middlePosition) {
        try {
            return Arrays.stream(middlePosition.getAdjacentPositions())
                    .filter(Position::isRoad)
                    .toArray(Position[]::new)[0];
        } catch (Exception e) {
            /*System.out.println("No adjacent road found");*/
            Position position =
                    Arrays.stream(middlePosition.getAdjacentPositions())
                            .filter(pos -> !pos.isObstacle())
                            .toArray(Position[]::new)[0];
            /*System.out.println(position);*/
            return position;
        }
    }

}
