package com.ibjava.codingtomorrow;

import javafx.scene.control.Alert;
import org.joda.time.LocalDateTime;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Utils {

    /**
     * Reads the text content of a file line-by-line into a String array.
     * @param file The file to be read
     * @return An array of the file's lines
     */
    public static String[] readFile(File file) {
        List<String> list = new ArrayList<>();
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNext()) {
                list.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
            return null;
        }
        return list.toArray(new String[0]);
    }

    /**
     * Writes content String to the specified file
     * @param file the file which will be created
     * @param content the content of the file
     */
    public static void writeToFile(File file, String content) {
        try {
            // if the parent folder does not exist then create it
            if (!file.getParentFile().isFile()) file.getParentFile().mkdirs();

            PrintWriter writer = new PrintWriter(file);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }

    public static class AlertUtil {
        /**
         * An Alert object with alertTitle and alertText is returned
         * thus facilitating working with optional dialogs.
         * @param title
         * @param content
         * @param type
         * @return
         */
        private static Alert createAlert(String title, String content, Alert.AlertType type) {
            Alert alert = new Alert(type);
            alert.setHeaderText(null);
            alert.setTitle(title);
            alert.setContentText(content);
            return alert;
        }

        public static void alert(String title, String content, Alert.AlertType type) {
            createAlert(title, content, type).showAndWait();
        }
    }

    /**
     * Utility functions for Java reflections
     */
    public static class ReflectionUtil {

        /**
         * Reflectively returns all declared fields of an object, including inherited ones as well
         * @param obj the object whose fields get returned
         * @param <T>
         * @return a list of Field objects
         */
        public static <T> List<Field> getFields(T obj) {
            // the class whose fields are inspected
            Class clazz = obj.getClass();
            List<Field> fieldList = new ArrayList<>();
            // stepping higher anf higher in the class tree-hierarchy until the last super class, Object class is reached
            while (clazz != Object.class) {
                // appending all the current class' declared fields to the list
                fieldList.addAll(Arrays.asList(clazz.getDeclaredFields()));
                clazz = clazz.getSuperclass();
            }
            return fieldList;
        }

        /**
         * Reflectively returns the value of an object's field by invoking the corresponding getter
         * @param field The field whose values is returned
         * @param obj The object whose field is reflected
         * @param <T>
         * @return The value of the parameter object's field
         */
        public static <T> Object callGetter(Field field, T obj) {
            try {
                // filtering all publicly available methods for the correct getter
                return Arrays
                        .stream(obj.getClass().getMethods())
                        .filter(method -> method.getName().startsWith("get") &&
                                method.getName().length() == (field.getName().length() + "get".length()) &&
                                method.getName().toLowerCase().endsWith(field.getName().toLowerCase()))
                        .toArray(Method[]::new)[0]
                        .invoke(obj); // after obtaining the correct getter, invoke it
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    public static String nowDateTimeToString() {
        LocalDateTime now = LocalDateTime.now();
        return "Y" + now.getYear() + "_M" + now.getMonthOfYear() + "_D" + now.getDayOfMonth() + "_H" +
                now.getHourOfDay() + "_m" + now.getMinuteOfHour() + "_s" + now.getSecondOfMinute();
    }

}
