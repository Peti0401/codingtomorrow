package com.ibjava.codingtomorrow.entities;

import com.ibjava.codingtomorrow.controlclasses.Direction;

/**
 * Class for representing entities, which are capable of moving, having the following extra fields:
 * -byte speed ranges from 0 to 3
 * -char direction <, >, v, ^
 * -char next_command 0, +, -, <, >
 */
public abstract class MovingEntity extends Entity {

    byte speed;
    Direction direction;
    String next_command;

    public MovingEntity() {}

    MovingEntity(int id, byte x, byte y, byte speed, Direction direction, String next_command) {
        super(id, x, y);
        this.speed = speed;
        this.direction = direction;
        this.next_command = next_command;
    }

    public byte getSpeed() {
        return speed;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getNext_command() {
        return next_command;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void move() {
        setPosition(getPos().translateToDirection(direction, speed));
    }

    @Override
    public String toString() {
        return "MovingEntity{" + "\n" +
                "\tspeed=" + speed + "\n" +
                "\tdirection=" + direction + "\n" +
                "\tnext_command='" + next_command + '\'' + "\n" +
                "\tid=" + id + "\n" +
                "\tpos=" + pos + "\n" +
                '}';
    }
}
