package com.ibjava.codingtomorrow.entities;

import com.ibjava.codingtomorrow.controlclasses.Board;
import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Train extends MovingEntity {

    public enum CompassPoint {
        NORTH, EAST, SOUTH, WEST
    }

    private static final int RETURN_PERIOD = 50;
    private static final int TRAIN_LENGTH = 10;

    private final int inTick;
    private final Position[] inPositions;
    private final int outTick;
    private final Position[] outPositions;
    private final CompassPoint compassPoint;


    public Train(int inTick, Position[] inPositions, int outTick, Position[] outPositions, Direction direction, CompassPoint compassPoint) {
        super(0, (byte) 0, (byte) 0, (byte) 3, direction, "");
        this.inTick = inTick;
        this.inPositions = inPositions;
        this.outTick = outTick;
        this.outPositions = outPositions;
        this.compassPoint = compassPoint;
    }

    public List<Position> getAllOccupiedPositions(int tickNum) {
        List<Position> positions = new ArrayList<>();
        int remainder = tickNum % RETURN_PERIOD;

        // if the train is on the map
        if (remainder >= inTick && remainder <= outTick) {
            int isInTheMapSince = remainder - inTick;
            int covered = 2 + isInTheMapSince * 3;
            // placing the train's head's position then backtracking
            Position trainHeadPosition = inPositions[1];
            for (int i = 0; i < isInTheMapSince; i++) {
                trainHeadPosition = trainHeadPosition.translateToDirectionWithoutOverlap(direction, speed);
            }
            positions.add(trainHeadPosition);

            // backtracking
            Position prevPos = trainHeadPosition;
            for (int i = 0; i < TRAIN_LENGTH - 1; i++) {
                prevPos = prevPos.translateToDirectionWithoutOverlap(direction, ((byte) -1));
                positions.add(prevPos);
            }
            // reverse the list so that the train's head is the last element
            Collections.reverse(positions);
            // remove invalid positions
            positions =  positions.stream()
                    .filter(Position::isValid)
                    .collect(Collectors.toList());
        } // else an empty list is returned
        return positions;
    }

    public Position getHead(int tickNum) {
        int remainder = tickNum % RETURN_PERIOD;
        if (remainder >= inTick && remainder <= outTick) {
            int isInTheMapSince = remainder - inTick;
            int covered = 2 + isInTheMapSince * 3;
            if (covered >= Board.getInstance().numCols() || covered >= Board.getInstance().numRows()) {
                return null;
            } else {
                List<Position> positions = getAllOccupiedPositions(tickNum);
                Position head = positions.get(positions.size() - 1); // last element
                return head;
            }
        } else {
            return null;
        }
    }

    public Position getTail(int tickNum) {
        int remainder = tickNum % RETURN_PERIOD;
        if (remainder >= inTick && remainder <= outTick) {
            int isInTheMapSince = remainder - inTick;
            int covered = 2 + isInTheMapSince * 3;
            if (covered < 10) {
                return null;
            } else {
                List<Position> positions = getAllOccupiedPositions(tickNum);
                Position tail = positions.get(0); // first element
                return tail;
            }
        } else {
            return null;
        }
    }

    public int getInTick() {
        return inTick;
    }

    public Position[] getInPositions() {
        return inPositions;
    }

    public int getOutTick() {
        return outTick;
    }

    public Position[] getOutPositions() {
        return outPositions;
    }

    public CompassPoint getCompassPoint() {
        return compassPoint;
    }
}
