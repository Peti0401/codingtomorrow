package com.ibjava.codingtomorrow.entities;

import com.ibjava.codingtomorrow.controlclasses.Position;

/**
 * Base class for representing entities, having the most general fields:
 * - id
 * - pos {x, y}
 */
public abstract class Entity {

    int id;
    Position pos; // coordinate on the BOARD_UI

    Entity() {}

    Entity(int id, Position pos) {
        this.id = id;
        this.pos = pos;
    }

    Entity(int id, byte x, byte y) {
        this.id = id;
        this.pos = new Position(x, y);
    }

    public int getId() {
        return id;
    }

    public Position getPos() {
        return pos;
    }

    public void setPosition(Position position) {
        this.pos = new Position(position);
    }

    @Override
    public String toString() {
        return "Entity{" + "\n" +
                "\tid=" + id + "\n" +
                "\tpos=" + pos + "\n" +
                '}';
    }
}
