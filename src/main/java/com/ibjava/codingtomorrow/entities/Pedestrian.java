package com.ibjava.codingtomorrow.entities;

import com.ibjava.codingtomorrow.controlclasses.Direction;

public class Pedestrian extends MovingEntity {

    public Pedestrian(int id, byte x, byte y, byte speed, Direction direction, String next_command) {
        super(id, x, y, speed, direction, next_command);
    }

    public Pedestrian(Pedestrian pedestrian) {
        super(pedestrian.getId(), pedestrian.getPos().getX(), pedestrian.getPos().getY(), pedestrian.getSpeed(), pedestrian.getDirection(), pedestrian.getNext_command());
    }

    @Override
    public String toString() {
        return "Pedestrian{" + "\n" +
                "\tspeed=" + speed + "\n" +
                "\tdirection=" + direction + "\n" +
                "\tnext_command='" + next_command + '\'' + "\n" +
                "\tid=" + id + "\n" +
                "\tpos=" + pos + "\n" +
                '}';
    }
}
