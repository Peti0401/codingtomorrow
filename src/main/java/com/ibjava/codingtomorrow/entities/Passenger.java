package com.ibjava.codingtomorrow.entities;

import com.ibjava.codingtomorrow.controlclasses.Position;

 public class Passenger extends Entity {

     Position dest_pos;
     int car_id;

     public Passenger(int id, byte x, byte y, byte dest_x, byte dest_y, int car_id) {
        super(id, x, y);
        this.dest_pos = new Position(dest_x, dest_y);
        this.car_id = car_id;
    }

     public Passenger(int id, Position pos, Position dest_pos, int car_id) {
        super(id, pos);
        this.dest_pos = dest_pos;
        this.car_id = car_id;
    }

     public Position getDest_pos() {
         return dest_pos;
     }

     public int getCar_id() {
         return car_id;
     }

     public boolean didReachDestination() {
         return dest_pos.equals(pos);
     }

     @Override
     public String toString() {
         return "Passenger{" + "\n" +
                 "\tdest_pos=" + dest_pos + "\n" +
                 "\tcar_id=" + car_id + "\n" +
                 "\tid=" + id + "\n" +
                 "\tpos=" + pos + "\n" +
                 '}';
     }
 }
