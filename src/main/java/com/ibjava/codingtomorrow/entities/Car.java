package com.ibjava.codingtomorrow.entities;

import com.ibjava.codingtomorrow.controlclasses.Direction;
import com.ibjava.codingtomorrow.controlclasses.Position;

import java.util.Arrays;

public class Car extends MovingEntity {

    private byte life; // ranges from 0 to 100
    private int transported; // number of transported passengers
    private int passenger_id; // id of current passenger

    public Car(int id, byte x, byte y, byte speed, Direction direction, String next_command, byte life, int transported, int passenger_id) {
        super(id, x, y, speed, direction, next_command);
        this.life = life;
        this.transported = transported;
        this.passenger_id = passenger_id;
    }

    public Car(Car car) {
        super(car.getId(), car.getPos().getX(), car.getPos().getY(), car.getSpeed(), car.getDirection(), car.getNext_command());
        this.life = car.getLife();
        this.transported = car.getTransported();
        this.passenger_id = car.getPassenger_id();
    }

    public byte getLife() {
        return life;
    }

    public int getTransported() {
        return transported;
    }

    public int getPassenger_id() {
        return passenger_id;
    }

    public boolean hasPassenger() {
        return passenger_id != 0;
    }

    public boolean hasPassengerOnRoute(Passenger[] passengers) {
        if (passenger_id == 0) return false;
        Passenger passenger = getPassenger(passengers);
        Position[] adjacentPositions = getPos().getAdjacentPositions();
        return Arrays
                .stream(adjacentPositions)
                .noneMatch(adjacentPosition -> adjacentPosition.equals(passenger.getPos()));
    }

    public Integer getMaxSpeed() {
        if (life < 60 && life >= 25) {
            return 2;
        } else if (life < 25) {
            return 1;
        } else {
            return 3;
        }
    }

    public Passenger getPassenger(Passenger[] passengers) {
        return Arrays
                .stream(passengers)
                .filter(p -> p.getId() == passenger_id)
                .toArray(Passenger[]::new)[0];
    }

    public void setPassenger_Id(int passenger_id) {
        this.passenger_id = passenger_id;
    }

    public boolean accelerate() {
        if (speed < getMaxSpeed()) {
            speed += 1;
            return true;
        }
        return false;
    }

    public boolean decelerate() {
        if (speed > 0) {
            speed -= 1;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Car{" + "\n" +
                "\tlife=" + life + "\n" +
                "\ttransported=" + transported + "\n" +
                "\tpassenger_id=" + passenger_id + "\n" +
                "\tspeed=" + speed + "\n" +
                "\tdirection=" + direction + "\n" +
                "\tnext_command='" + next_command + '\'' + "\n" +
                "\tid=" + id + "\n" +
                "\tpos=" + pos + "\n" +
                '}';
    }
}
