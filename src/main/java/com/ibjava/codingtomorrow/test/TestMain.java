package com.ibjava.codingtomorrow.test;

import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.history.HistoryManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TestMain {

    private final static Map<Integer, Position> START_POS_MAP = new HashMap<Integer, Position>()
    {
        {
            put(20, new Position(49, 25));
            put(21, new Position(27, 30));
            put(22, new Position(29, 7));
            put(23, new Position(23, 15));
            put(24, new Position(15, 50));
            put(25, new Position(41, 8));
            put(26, new Position(36, 44));
            put(27, new Position(51, 3));
            put(28, new Position(44, 30));
            put(29, new Position(57, 37));
        }
    };

    private final static int TEST_DURATION_MINS = 10; // the test will run for this many minutes
    private final static int TEST_RUNS = 25;
    private final static int RUN_TIME_LIMIT_SECONDS = 300; // 5 minutes max by server
    private final static boolean SAVE_RUNS_INDIVIDUALLY = true;

    public static void main(String[] args) throws Exception {
        runTest();
    }

    private static void runTest() {
        // uncomment this to test every single starting position
        TerminalTest test = new TerminalTest();

        // choose random positions
        Integer[] randomPositionCodes = {25};
        // uncomment this to test the chosen random positions
        //TerminalTest test = new TerminalTest(Arrays.stream(randomPositionCodes).map(START_POS_MAP::get).collect(Collectors.toList()));

        //TerminalTest test = new TerminalTest(20);

        test.start(SAVE_RUNS_INDIVIDUALLY);
        HistoryManager.saveTerminalTestToHistory(test);
    }

}
