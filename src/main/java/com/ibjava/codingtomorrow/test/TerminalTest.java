package com.ibjava.codingtomorrow.test;

import com.ibjava.codingtomorrow.Main;
import com.ibjava.codingtomorrow.Utils;
import com.ibjava.codingtomorrow.communication.NavigationSystem;
import com.ibjava.codingtomorrow.controlclasses.Game;
import com.ibjava.codingtomorrow.controlclasses.Position;
import com.ibjava.codingtomorrow.controlclasses.Response;
import com.ibjava.codingtomorrow.controlclasses.Tick;
import com.ibjava.codingtomorrow.history.HistoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class TerminalTest {

    private enum TestType {
        FIXED_NUM,
        FIXED_POSITIONS,
        ALL_POSITIONS
    }

    private TestType testType;
    private int numberOfRuns; // this many runs will be performed
    private int runTimeLimitSeconds; // max time of a run
    private final static int DEFAULT_RUN_TIME_SECONDS = 300;
    private List<Game> gameList = new ArrayList<>();
    private boolean isFinished = false;
    private int mostPoints = -1;
    private long testMinutes = 0;
    private String testStart;
    private List<Position> targetPositions;
    private List<Position> positions = new ArrayList<>();


    /**
     * Fixed number of runs with random positions given by the server
     * @param numberOfRuns
     */
    TerminalTest(int numberOfRuns) {
        testType = TestType.FIXED_NUM;
        this.numberOfRuns = numberOfRuns;
        this.runTimeLimitSeconds = DEFAULT_RUN_TIME_SECONDS;
        this.testStart = Utils.nowDateTimeToString();
    }

    /**
     * Tests each parameter target position once
     * @param targetPositions
     */
    TerminalTest(List<Position> targetPositions) {
        testType = TestType.FIXED_POSITIONS;
        numberOfRuns = targetPositions.size();
        runTimeLimitSeconds = DEFAULT_RUN_TIME_SECONDS;
        this.testStart = Utils.nowDateTimeToString();
        this.targetPositions = targetPositions;
    }

    /**
     * Tests each start position once
     */
    TerminalTest() {
        testType = TestType.ALL_POSITIONS;
        numberOfRuns = 10;
        runTimeLimitSeconds = DEFAULT_RUN_TIME_SECONDS;
        this.testStart = Utils.nowDateTimeToString();
    }

    private String getTestInfo() {
        long testTimeSeconds = numberOfRuns * runTimeLimitSeconds;
        return "numberOfRuns = " + numberOfRuns + "\n" +
                "runTimeLimitSeconds = " + runTimeLimitSeconds + "\n" +
                "testTimeSeconds = " + testTimeSeconds;
    }

    public void start(boolean saveGameToHistory) {
        long startTime = System.currentTimeMillis();
        isFinished = false;
        System.out.println(getTestInfo());
        for (int i = 0; i < numberOfRuns; i++) {
            System.out.println("=====================================================");
            System.out.println("=======================" + (i+1) + "/" + numberOfRuns + "========================");
            System.out.println("=====================================================");
            System.out.println("--------------------------- " + mostPoints + "---------------------------");
            ExecutorService executorService = Executors.newFixedThreadPool(4);
            Future<?> future = executorService.submit(() ->
            {
                switch (testType) {
                    case FIXED_NUM:
                        fixedNum(saveGameToHistory);
                        break;
                    case FIXED_POSITIONS:
                        fixedPositions(saveGameToHistory);
                        break;
                    case ALL_POSITIONS:
                        allPositions(saveGameToHistory);
                        break;
                }
            });

            try {
                future.get(runTimeLimitSeconds, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                System.err.println("job was interrupted");
            } catch (ExecutionException e) {
                System.err.println("error in execution");
            } catch (TimeoutException e) {
                future.cancel(true);
                NavigationSystem.getInstance().getClient().stopConnection();
                System.err.println("timeout");
            } finally {
                try {
                    if(!executorService.awaitTermination(3, TimeUnit.SECONDS)){
                        // force them to quit by interrupting
                        executorService.shutdownNow();
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    System.err.println("shutdown interrupted");
                }
            }
            if (!gameList.isEmpty()) {
                Game currentGame = gameList.get(gameList.size() - 1);
                int currentScore = currentGame.getNumTransported();
                if (currentScore > mostPoints) mostPoints = currentScore;
            }
        }
        isFinished = true;
        long endTime = System.currentTimeMillis();
        long elapsedMillis = endTime - startTime;
        long elapsedSeconds = elapsedMillis / 1000;
        long elapsedMinutes = elapsedSeconds / 60;

        testMinutes = elapsedMinutes;
        System.out.println("Test completed in " + testMinutes + " minutes");
    }

    private void fixedNum(boolean saveGameToHistory) {
        Game game = new Game();
        NavigationSystem navi = NavigationSystem.getInstance();
        try {
            Tick tick = navi.startCommunication();
            while (true) {
                Response resp = NavigationSystem.navigate(tick);
                game.add(tick, resp);
                tick = navi.respond(resp);
            }
        } catch (Exception e) {
            System.out.println(game);
            if (saveGameToHistory) HistoryManager.saveGameToHistory(game);
            if (!game.getTicks().isEmpty()) gameList.add(game);
            e.printStackTrace();
            navi.getClient().stopConnection();
        }
    }

    private void fixedPositions(boolean saveGameToHistory) {
        Game game = new Game();
        NavigationSystem navi = NavigationSystem.getInstance();
        try {
            Tick tick = navi.startCommunication();
            Position startPosition = tick.getOurCar().getPos();
            if (!targetPositions.contains(startPosition)) {
                numberOfRuns++;
                NavigationSystem.getInstance().getClient().stopConnection();
            } else {
                System.out.println("Found position: " + startPosition);
                targetPositions.remove(startPosition);
                while (true) {
                    Response resp = NavigationSystem.navigate(tick);
                    game.add(tick, resp);
                    tick = navi.respond(resp);
                }
            }
        } catch (Exception e) {
            System.out.println(game);
            if (saveGameToHistory && !game.getTicks().isEmpty()) HistoryManager.saveGameToHistory(game);
            if (!game.getTicks().isEmpty()) gameList.add(game);
            e.printStackTrace();
            navi.getClient().stopConnection();
        }
    }

    private void allPositions(boolean saveGameToHistory) {
        Game game = new Game();
        NavigationSystem navi = NavigationSystem.getInstance();
        try {
            Tick tick = navi.startCommunication();
            Position startPosition = tick.getOurCar().getPos();
            if (positions.contains(startPosition)) {
                numberOfRuns++;
                NavigationSystem.getInstance().getClient().stopConnection();
            } else {
                System.out.println("Found new position: " + startPosition);
                positions.add(startPosition);
                while (true) {
                    Response resp = NavigationSystem.navigate(tick);
                    game.add(tick, resp);
                    tick = navi.respond(resp);
                }
            }
        } catch (Exception e) {
            System.out.println(game);
            if (saveGameToHistory && !game.getTicks().isEmpty()) HistoryManager.saveGameToHistory(game);
            if (!game.getTicks().isEmpty()) gameList.add(game);
            e.printStackTrace();
            navi.getClient().stopConnection();
        }
    }

    public void start() {
        start(false);
    }

    public boolean isFinished() {
        return isFinished;
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public int getNumberOfRuns() {
        return numberOfRuns;
    }

    public int getRunTimeLimitSeconds() {
        return runTimeLimitSeconds;
    }

    public int getMostPoints() {
        return mostPoints;
    }

    public long getTestMinutes() {
        return testMinutes;
    }

    public String getTestStart() {
        return testStart;
    }

    public List<Position> getPositions() {
        return positions;
    }

    @Override
    public String toString() {
        return "TerminalTest_" + testStart + "(" + numberOfRuns + ")[" + mostPoints + "]";
    }
}
